use std::path::PathBuf;

fn main() {
    let dir: PathBuf = ["tree-sitter-uvl", "src"].iter().collect();

    cc::Build::new()
        .include(&dir)
        .file(dir.join("parser.c"))
        .compile("tree-sitter-uvl");
    cc::Build::new()
        .include(&dir)
        .cpp(true)
        .file(dir.join("scanner.cc"))
        .compile("tree-sitter-uvl-scanner");
}
