# UVLS - Universal Variability Language Server

This is a language server for UVL(Universal Variability Language)

## Features
Currently supports

- completions
- syntax highlighting
- error messages

## Requirements
- Rust 1.65+
- Git  

You need to build UVLS from [source](https://codeberg.org/caradhras/uvl-lsp) and make sure its in path.  
To build:
```
git clone https://codeberg.org/caradhras/uvl-lsp
cd  uvl-lsp
cargo build --release
```
The binary will be under uvl-lsp/target/release
## Extension Settings

* `uvls.path`: Path to the server executable eg. uvls


