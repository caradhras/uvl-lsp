"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const vscode_1 = require("vscode");
const path = require("path");
const os = require("os");
const which = require("which");
const fs = require("fs");
const node_1 = require("vscode-languageclient/node");
let client;
let outputChannel;
// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
async function uvlsPath() {
    const configuration = vscode_1.workspace.getConfiguration("uvls");
    var uvlsPath = configuration.get("path", null);
    if (!uvlsPath) {
        uvlsPath = which.sync('uvls', { nothrow: true });
    }
    else if (uvlsPath.startsWith("~")) {
        uvlsPath = path.join(os.homedir(), uvlsPath.substring(1));
    }
    else if (!path.isAbsolute(uvlsPath)) {
        uvlsPath = which.sync(uvlsPath, { nothrow: true });
    }
    const uvlsPathExists = uvlsPath !== null && fs.existsSync(uvlsPath);
    var message = null;
    if (uvlsPath && uvlsPathExists) {
        try {
            fs.accessSync(uvlsPath, fs.constants.R_OK | fs.constants.X_OK);
        }
        catch {
            message = `\`uvls.path\` ${uvlsPath} is not an executable`;
        }
        const stat = fs.statSync(uvlsPath);
        if (!stat.isFile()) {
            message = `\`uvls.path\` ${uvlsPath} is not a file`;
        }
    }
    if (message === null) {
        if (!uvlsPath) {
            message = "Couldn't find UVL Language Server (UVLS) executable, please specify it under \`uvls.path\`";
        }
        else if (!uvlsPathExists) {
            message = `Couldn't find UVL Language Server (UVLS) executable at ${uvlsPath}`;
        }
    }
    if (message) {
        const response = await vscode_1.window.showWarningMessage(message);
    }
    return uvlsPath;
}
async function runServer(path) {
    outputChannel = vscode.window.createOutputChannel("UVL Language Server");
    const serverOptions = {
        command: path, // Replace with your own command.,
    };
    const clientOptions = {
        documentSelector: [{ scheme: "file", language: "uvl" }],
        outputChannel
    };
    client = new node_1.LanguageClient('uvls', serverOptions, clientOptions);
    client.start();
}
async function activate(context) {
    const path = await uvlsPath();
    if (!path) {
        vscode_1.window.showWarningMessage("Couldn't find Zig Language Server (ZLS) executable");
    }
    else {
        await runServer(path);
    }
}
exports.activate = activate;
// This method is called when your extension is deactivated
function deactivate() {
    if (!client) {
        return undefined;
    }
    return client.stop();
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map