// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { workspace, window } from 'vscode';
import * as path from "path";
import * as os from "os";
import * as which from "which";
import * as fs from "fs";
import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	TransportKind
} from 'vscode-languageclient/node';

let client: LanguageClient;
let outputChannel;
// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed

async function uvlsPath(){
	const configuration = workspace.getConfiguration("uvls");
	var uvlsPath = configuration.get<string | null>("path", null);

	if (!uvlsPath) {
		uvlsPath = which.sync('uvls', { nothrow: true });
	} else if (uvlsPath.startsWith("~")) {
		uvlsPath = path.join(os.homedir(), uvlsPath.substring(1));
	} else if (!path.isAbsolute(uvlsPath)) {
		uvlsPath = which.sync(uvlsPath, { nothrow: true });
	}
	const uvlsPathExists = uvlsPath !== null && fs.existsSync(uvlsPath);
	var message: string | null = null;
	if (uvlsPath && uvlsPathExists) {
		try {
			fs.accessSync(uvlsPath, fs.constants.R_OK | fs.constants.X_OK);
		} catch {
			message = `\`uvls.path\` ${uvlsPath} is not an executable`;
		}
		const stat = fs.statSync(uvlsPath);
		if (!stat.isFile()) {
			message = `\`uvls.path\` ${uvlsPath} is not a file`;
		}
	}
	if (message === null) {
		if (!uvlsPath) {
			message = "Couldn't find UVL Language Server (UVLS) executable, please specify it under \`uvls.path\`";
		} else if (!uvlsPathExists) {
			message = `Couldn't find UVL Language Server (UVLS) executable at ${uvlsPath}`;
		}
	}
	if (message) {
		const response = await window.showWarningMessage(message);
	}
	return uvlsPath

}
async function runServer(path:string){
	outputChannel = vscode.window.createOutputChannel("UVL Language Server");
	const serverOptions: ServerOptions = {
		command: path, // Replace with your own command.,
	};

	const clientOptions: LanguageClientOptions = {
		documentSelector: [{ scheme: "file", language: "uvl" }],
		outputChannel
	};
	client = new LanguageClient('uvls', serverOptions, clientOptions);
	client.start();
}

export async function activate(context: vscode.ExtensionContext) {
	const path = await uvlsPath();
	if (!path){
		window.showWarningMessage("Couldn't find Zig Language Server (ZLS) executable");
	}
	else{
		await runServer(path);
	}

	

	
	



}

// This method is called when your extension is deactivated
export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
