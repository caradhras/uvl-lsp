

[
 "namespace"
 "as"
 "constraint"
 ]@keyword
(comment) @comment

(string) @string

(int) @number

(bool) @boolean

(number) @float
[ "(" ")" "[" "]" "{" "}"] @punctuation.bracket

[
    (group_mode)
    (constraints)
    (imports)
    (include)
    (features)
] @keyword

[
    (op)
] @operator




(attribute_value name:(_)@field)
(blk
  header:[(features) (name) (group_mode)]
  [(blk
    header: (name) @type
  )(blk
    header: (ref path:(_) @include) 
  )]
)
(source_file
    (blk
        header:(imports)
        [(blk
            header: (ref path:(_)@include alias:(_)?@include )
         )
         (blk
            header:(name)@include
          )

        ]
    )
)
(source_file
    (blk
        header:(namespace name:(_) @namespace)
        )
)
(constraint
  (path)@type
)
(equation
  (path)@field
)
(numeric
  (path)@field
)




