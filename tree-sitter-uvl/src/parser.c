#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 272
#define LARGE_STATE_COUNT 13
#define SYMBOL_COUNT 87
#define ALIAS_COUNT 3
#define TOKEN_COUNT 51
#define EXTERNAL_TOKEN_COUNT 7
#define FIELD_COUNT 15
#define MAX_ALIAS_SEQUENCE_LENGTH 6
#define PRODUCTION_ID_COUNT 30

enum {
  sym_name = 1,
  anon_sym_LBRACE = 2,
  anon_sym_COMMA = 3,
  anon_sym_RBRACE = 4,
  anon_sym_as = 5,
  anon_sym_namespace = 6,
  anon_sym_LBRACK = 7,
  anon_sym_DOT_DOT = 8,
  anon_sym_STAR = 9,
  anon_sym_RBRACK = 10,
  anon_sym_constraint = 11,
  anon_sym_constraints = 12,
  anon_sym_PIPE = 13,
  anon_sym_AMP = 14,
  anon_sym_EQ_GT = 15,
  anon_sym_LT_EQ_GT = 16,
  anon_sym_BANG = 17,
  anon_sym_GT = 18,
  anon_sym_LT = 19,
  anon_sym_EQ = 20,
  anon_sym_PLUS = 21,
  anon_sym_DASH = 22,
  anon_sym_SLASH = 23,
  anon_sym_LPAREN = 24,
  anon_sym_RPAREN = 25,
  anon_sym_avg = 26,
  anon_sym_sum = 27,
  anon_sym_true = 28,
  anon_sym_false = 29,
  anon_sym_DQUOTE = 30,
  aux_sym_string_token1 = 31,
  sym_number = 32,
  sym_comment = 33,
  anon_sym_DOT = 34,
  anon_sym_or = 35,
  anon_sym_alternative = 36,
  anon_sym_mandatory = 37,
  anon_sym_optional = 38,
  anon_sym_SMT_DASHlevel = 39,
  anon_sym_SAT_DASHlevel = 40,
  anon_sym_group_DASHcardinality = 41,
  anon_sym_feature_DASHcardinality = 42,
  anon_sym_aggregate_DASHfunction = 43,
  sym_imports = 44,
  sym_features = 45,
  sym_include = 46,
  sym_int = 47,
  sym__indent = 48,
  sym__dedent = 49,
  sym__newline = 50,
  sym_source_file = 51,
  sym_blk = 52,
  sym_attributes = 53,
  sym__header = 54,
  sym_ref = 55,
  sym_namespace = 56,
  sym_incomplete_namespace = 57,
  sym_incomplete_ref = 58,
  sym_cardinality = 59,
  sym_attribute_constraint = 60,
  sym_attribute_constraints = 61,
  sym_attribute_value = 62,
  sym__attribute = 63,
  sym__value = 64,
  sym__expr = 65,
  sym_constraint = 66,
  sym_equation = 67,
  sym_numeric = 68,
  sym_nested_expr = 69,
  sym__aggregate = 70,
  sym_vector = 71,
  sym_bool = 72,
  sym_string = 73,
  sym_lang_lvl = 74,
  sym_path = 75,
  sym_group_mode = 76,
  sym_major_lvl = 77,
  sym_minor_lvl = 78,
  sym_constraints = 79,
  sym__any_name = 80,
  aux_sym_source_file_repeat1 = 81,
  aux_sym_attributes_repeat1 = 82,
  aux_sym_attribute_constraints_repeat1 = 83,
  aux_sym_vector_repeat1 = 84,
  aux_sym_lang_lvl_repeat1 = 85,
  aux_sym_path_repeat1 = 86,
  alias_sym_attrib_expr = 87,
  alias_sym_expr = 88,
  alias_sym_tail = 89,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [sym_name] = "name",
  [anon_sym_LBRACE] = "{",
  [anon_sym_COMMA] = ",",
  [anon_sym_RBRACE] = "}",
  [anon_sym_as] = "as",
  [anon_sym_namespace] = "namespace",
  [anon_sym_LBRACK] = "[",
  [anon_sym_DOT_DOT] = "..",
  [anon_sym_STAR] = "*",
  [anon_sym_RBRACK] = "]",
  [anon_sym_constraint] = "constraint",
  [anon_sym_constraints] = "constraints",
  [anon_sym_PIPE] = "op",
  [anon_sym_AMP] = "op",
  [anon_sym_EQ_GT] = "op",
  [anon_sym_LT_EQ_GT] = "op",
  [anon_sym_BANG] = "op",
  [anon_sym_GT] = "op",
  [anon_sym_LT] = "op",
  [anon_sym_EQ] = "op",
  [anon_sym_PLUS] = "op",
  [anon_sym_DASH] = "op",
  [anon_sym_SLASH] = "op",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [anon_sym_avg] = "func",
  [anon_sym_sum] = "func",
  [anon_sym_true] = "true",
  [anon_sym_false] = "false",
  [anon_sym_DQUOTE] = "\"",
  [aux_sym_string_token1] = "string_token1",
  [sym_number] = "number",
  [sym_comment] = "comment",
  [anon_sym_DOT] = ".",
  [anon_sym_or] = "or",
  [anon_sym_alternative] = "alternative",
  [anon_sym_mandatory] = "mandatory",
  [anon_sym_optional] = "optional",
  [anon_sym_SMT_DASHlevel] = "SMT-level",
  [anon_sym_SAT_DASHlevel] = "SAT-level",
  [anon_sym_group_DASHcardinality] = "group-cardinality",
  [anon_sym_feature_DASHcardinality] = "feature-cardinality",
  [anon_sym_aggregate_DASHfunction] = "aggregate-function",
  [sym_imports] = "imports",
  [sym_features] = "features",
  [sym_include] = "include",
  [sym_int] = "int",
  [sym__indent] = "_indent",
  [sym__dedent] = "_dedent",
  [sym__newline] = "_newline",
  [sym_source_file] = "source_file",
  [sym_blk] = "blk",
  [sym_attributes] = "attributes",
  [sym__header] = "_header",
  [sym_ref] = "ref",
  [sym_namespace] = "namespace",
  [sym_incomplete_namespace] = "incomplete_namespace",
  [sym_incomplete_ref] = "incomplete_ref",
  [sym_cardinality] = "cardinality",
  [sym_attribute_constraint] = "attribute_constraint",
  [sym_attribute_constraints] = "attribute_constraints",
  [sym_attribute_value] = "attribute_value",
  [sym__attribute] = "_attribute",
  [sym__value] = "_value",
  [sym__expr] = "_expr",
  [sym_constraint] = "constraint",
  [sym_equation] = "equation",
  [sym_numeric] = "numeric",
  [sym_nested_expr] = "nested_expr",
  [sym__aggregate] = "_aggregate",
  [sym_vector] = "vector",
  [sym_bool] = "bool",
  [sym_string] = "string",
  [sym_lang_lvl] = "lang_lvl",
  [sym_path] = "path",
  [sym_group_mode] = "group_mode",
  [sym_major_lvl] = "major_lvl",
  [sym_minor_lvl] = "minor_lvl",
  [sym_constraints] = "constraints",
  [sym__any_name] = "_any_name",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym_attributes_repeat1] = "attributes_repeat1",
  [aux_sym_attribute_constraints_repeat1] = "attribute_constraints_repeat1",
  [aux_sym_vector_repeat1] = "vector_repeat1",
  [aux_sym_lang_lvl_repeat1] = "lang_lvl_repeat1",
  [aux_sym_path_repeat1] = "path_repeat1",
  [alias_sym_attrib_expr] = "attrib_expr",
  [alias_sym_expr] = "expr",
  [alias_sym_tail] = "tail",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [sym_name] = sym_name,
  [anon_sym_LBRACE] = anon_sym_LBRACE,
  [anon_sym_COMMA] = anon_sym_COMMA,
  [anon_sym_RBRACE] = anon_sym_RBRACE,
  [anon_sym_as] = anon_sym_as,
  [anon_sym_namespace] = anon_sym_namespace,
  [anon_sym_LBRACK] = anon_sym_LBRACK,
  [anon_sym_DOT_DOT] = anon_sym_DOT_DOT,
  [anon_sym_STAR] = anon_sym_STAR,
  [anon_sym_RBRACK] = anon_sym_RBRACK,
  [anon_sym_constraint] = anon_sym_constraint,
  [anon_sym_constraints] = anon_sym_constraints,
  [anon_sym_PIPE] = anon_sym_PIPE,
  [anon_sym_AMP] = anon_sym_PIPE,
  [anon_sym_EQ_GT] = anon_sym_PIPE,
  [anon_sym_LT_EQ_GT] = anon_sym_PIPE,
  [anon_sym_BANG] = anon_sym_PIPE,
  [anon_sym_GT] = anon_sym_PIPE,
  [anon_sym_LT] = anon_sym_PIPE,
  [anon_sym_EQ] = anon_sym_PIPE,
  [anon_sym_PLUS] = anon_sym_PIPE,
  [anon_sym_DASH] = anon_sym_PIPE,
  [anon_sym_SLASH] = anon_sym_PIPE,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [anon_sym_avg] = anon_sym_avg,
  [anon_sym_sum] = anon_sym_avg,
  [anon_sym_true] = anon_sym_true,
  [anon_sym_false] = anon_sym_false,
  [anon_sym_DQUOTE] = anon_sym_DQUOTE,
  [aux_sym_string_token1] = aux_sym_string_token1,
  [sym_number] = sym_number,
  [sym_comment] = sym_comment,
  [anon_sym_DOT] = anon_sym_DOT,
  [anon_sym_or] = anon_sym_or,
  [anon_sym_alternative] = anon_sym_alternative,
  [anon_sym_mandatory] = anon_sym_mandatory,
  [anon_sym_optional] = anon_sym_optional,
  [anon_sym_SMT_DASHlevel] = anon_sym_SMT_DASHlevel,
  [anon_sym_SAT_DASHlevel] = anon_sym_SAT_DASHlevel,
  [anon_sym_group_DASHcardinality] = anon_sym_group_DASHcardinality,
  [anon_sym_feature_DASHcardinality] = anon_sym_feature_DASHcardinality,
  [anon_sym_aggregate_DASHfunction] = anon_sym_aggregate_DASHfunction,
  [sym_imports] = sym_imports,
  [sym_features] = sym_features,
  [sym_include] = sym_include,
  [sym_int] = sym_int,
  [sym__indent] = sym__indent,
  [sym__dedent] = sym__dedent,
  [sym__newline] = sym__newline,
  [sym_source_file] = sym_source_file,
  [sym_blk] = sym_blk,
  [sym_attributes] = sym_attributes,
  [sym__header] = sym__header,
  [sym_ref] = sym_ref,
  [sym_namespace] = sym_namespace,
  [sym_incomplete_namespace] = sym_incomplete_namespace,
  [sym_incomplete_ref] = sym_incomplete_ref,
  [sym_cardinality] = sym_cardinality,
  [sym_attribute_constraint] = sym_attribute_constraint,
  [sym_attribute_constraints] = sym_attribute_constraints,
  [sym_attribute_value] = sym_attribute_value,
  [sym__attribute] = sym__attribute,
  [sym__value] = sym__value,
  [sym__expr] = sym__expr,
  [sym_constraint] = sym_constraint,
  [sym_equation] = sym_equation,
  [sym_numeric] = sym_numeric,
  [sym_nested_expr] = sym_nested_expr,
  [sym__aggregate] = sym__aggregate,
  [sym_vector] = sym_vector,
  [sym_bool] = sym_bool,
  [sym_string] = sym_string,
  [sym_lang_lvl] = sym_lang_lvl,
  [sym_path] = sym_path,
  [sym_group_mode] = sym_group_mode,
  [sym_major_lvl] = sym_major_lvl,
  [sym_minor_lvl] = sym_minor_lvl,
  [sym_constraints] = sym_constraints,
  [sym__any_name] = sym__any_name,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym_attributes_repeat1] = aux_sym_attributes_repeat1,
  [aux_sym_attribute_constraints_repeat1] = aux_sym_attribute_constraints_repeat1,
  [aux_sym_vector_repeat1] = aux_sym_vector_repeat1,
  [aux_sym_lang_lvl_repeat1] = aux_sym_lang_lvl_repeat1,
  [aux_sym_path_repeat1] = aux_sym_path_repeat1,
  [alias_sym_attrib_expr] = alias_sym_attrib_expr,
  [alias_sym_expr] = alias_sym_expr,
  [alias_sym_tail] = alias_sym_tail,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [sym_name] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_LBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COMMA] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_as] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_namespace] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_STAR] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_constraint] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_constraints] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_AMP] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_EQ_GT] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_LT_EQ_GT] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_BANG] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_EQ] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_PLUS] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DASH] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_SLASH] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_avg] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_sum] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_true] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_false] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_string_token1] = {
    .visible = false,
    .named = false,
  },
  [sym_number] = {
    .visible = true,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_or] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_alternative] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_mandatory] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_optional] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SMT_DASHlevel] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SAT_DASHlevel] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_group_DASHcardinality] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_feature_DASHcardinality] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_aggregate_DASHfunction] = {
    .visible = true,
    .named = false,
  },
  [sym_imports] = {
    .visible = true,
    .named = true,
  },
  [sym_features] = {
    .visible = true,
    .named = true,
  },
  [sym_include] = {
    .visible = true,
    .named = true,
  },
  [sym_int] = {
    .visible = true,
    .named = true,
  },
  [sym__indent] = {
    .visible = false,
    .named = true,
  },
  [sym__dedent] = {
    .visible = false,
    .named = true,
  },
  [sym__newline] = {
    .visible = false,
    .named = true,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym_blk] = {
    .visible = true,
    .named = true,
  },
  [sym_attributes] = {
    .visible = true,
    .named = true,
  },
  [sym__header] = {
    .visible = false,
    .named = true,
  },
  [sym_ref] = {
    .visible = true,
    .named = true,
  },
  [sym_namespace] = {
    .visible = true,
    .named = true,
  },
  [sym_incomplete_namespace] = {
    .visible = true,
    .named = true,
  },
  [sym_incomplete_ref] = {
    .visible = true,
    .named = true,
  },
  [sym_cardinality] = {
    .visible = true,
    .named = true,
  },
  [sym_attribute_constraint] = {
    .visible = true,
    .named = true,
  },
  [sym_attribute_constraints] = {
    .visible = true,
    .named = true,
  },
  [sym_attribute_value] = {
    .visible = true,
    .named = true,
  },
  [sym__attribute] = {
    .visible = false,
    .named = true,
  },
  [sym__value] = {
    .visible = false,
    .named = true,
  },
  [sym__expr] = {
    .visible = false,
    .named = true,
  },
  [sym_constraint] = {
    .visible = true,
    .named = true,
  },
  [sym_equation] = {
    .visible = true,
    .named = true,
  },
  [sym_numeric] = {
    .visible = true,
    .named = true,
  },
  [sym_nested_expr] = {
    .visible = true,
    .named = true,
  },
  [sym__aggregate] = {
    .visible = false,
    .named = true,
  },
  [sym_vector] = {
    .visible = true,
    .named = true,
  },
  [sym_bool] = {
    .visible = true,
    .named = true,
  },
  [sym_string] = {
    .visible = true,
    .named = true,
  },
  [sym_lang_lvl] = {
    .visible = true,
    .named = true,
  },
  [sym_path] = {
    .visible = true,
    .named = true,
  },
  [sym_group_mode] = {
    .visible = true,
    .named = true,
  },
  [sym_major_lvl] = {
    .visible = true,
    .named = true,
  },
  [sym_minor_lvl] = {
    .visible = true,
    .named = true,
  },
  [sym_constraints] = {
    .visible = true,
    .named = true,
  },
  [sym__any_name] = {
    .visible = false,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_attributes_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_attribute_constraints_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_vector_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_lang_lvl_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_path_repeat1] = {
    .visible = false,
    .named = false,
  },
  [alias_sym_attrib_expr] = {
    .visible = true,
    .named = true,
  },
  [alias_sym_expr] = {
    .visible = true,
    .named = true,
  },
  [alias_sym_tail] = {
    .visible = true,
    .named = true,
  },
};

enum {
  field_alias = 1,
  field_attribs = 2,
  field_begin = 3,
  field_child = 4,
  field_end = 5,
  field_first = 6,
  field_header = 7,
  field_last = 8,
  field_lhs = 9,
  field_name = 10,
  field_op = 11,
  field_path = 12,
  field_rhs = 13,
  field_tail = 14,
  field_value = 15,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_alias] = "alias",
  [field_attribs] = "attribs",
  [field_begin] = "begin",
  [field_child] = "child",
  [field_end] = "end",
  [field_first] = "first",
  [field_header] = "header",
  [field_last] = "last",
  [field_lhs] = "lhs",
  [field_name] = "name",
  [field_op] = "op",
  [field_path] = "path",
  [field_rhs] = "rhs",
  [field_tail] = "tail",
  [field_value] = "value",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [2] = {.index = 0, .length = 3},
  [4] = {.index = 3, .length = 1},
  [5] = {.index = 4, .length = 1},
  [6] = {.index = 5, .length = 1},
  [7] = {.index = 6, .length = 1},
  [8] = {.index = 7, .length = 2},
  [9] = {.index = 9, .length = 1},
  [10] = {.index = 10, .length = 1},
  [11] = {.index = 11, .length = 1},
  [12] = {.index = 12, .length = 3},
  [13] = {.index = 12, .length = 3},
  [14] = {.index = 15, .length = 2},
  [15] = {.index = 17, .length = 1},
  [16] = {.index = 18, .length = 2},
  [17] = {.index = 20, .length = 3},
  [19] = {.index = 23, .length = 2},
  [21] = {.index = 25, .length = 4},
  [22] = {.index = 29, .length = 2},
  [23] = {.index = 31, .length = 4},
  [24] = {.index = 35, .length = 5},
  [25] = {.index = 40, .length = 3},
  [27] = {.index = 43, .length = 1},
  [28] = {.index = 43, .length = 1},
  [29] = {.index = 44, .length = 1},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_first, 0, .inherited = true},
    {field_last, 0, .inherited = true},
    {field_op, 0, .inherited = true},
  [3] =
    {field_path, 0},
  [4] =
    {field_name, 1},
  [5] =
    {field_op, 0},
  [6] =
    {field_header, 0},
  [7] =
    {field_attribs, 1},
    {field_header, 0},
  [9] =
    {field_tail, 1},
  [10] =
    {field_end, 1},
  [11] =
    {field_name, 0},
  [12] =
    {field_lhs, 0},
    {field_op, 1},
    {field_rhs, 2},
  [15] =
    {field_alias, 2},
    {field_path, 0},
  [17] =
    {field_tail, 2},
  [18] =
    {field_last, 2},
    {field_op, 0},
  [20] =
    {field_child, 2},
    {field_child, 3},
    {field_header, 0},
  [23] =
    {field_name, 0},
    {field_value, 1},
  [25] =
    {field_attribs, 1},
    {field_child, 2},
    {field_child, 3},
    {field_header, 0},
  [29] =
    {field_begin, 1},
    {field_end, 3},
  [31] =
    {field_child, 2},
    {field_child, 3},
    {field_child, 4},
    {field_header, 0},
  [35] =
    {field_attribs, 1},
    {field_child, 2},
    {field_child, 3},
    {field_child, 4},
    {field_header, 0},
  [40] =
    {field_first, 2},
    {field_last, 4},
    {field_op, 0},
  [43] =
    {field_tail, 3},
  [44] =
    {field_tail, 4},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
  [1] = {
    [0] = alias_sym_expr,
  },
  [3] = {
    [0] = sym_name,
  },
  [9] = {
    [1] = alias_sym_tail,
  },
  [12] = {
    [1] = anon_sym_PIPE,
  },
  [15] = {
    [2] = alias_sym_tail,
  },
  [18] = {
    [1] = alias_sym_expr,
  },
  [20] = {
    [0] = alias_sym_attrib_expr,
  },
  [26] = {
    [2] = alias_sym_expr,
  },
  [27] = {
    [3] = alias_sym_tail,
  },
  [28] = {
    [2] = alias_sym_expr,
    [3] = alias_sym_tail,
  },
  [29] = {
    [2] = alias_sym_expr,
    [4] = alias_sym_tail,
  },
};

static const uint16_t ts_non_terminal_alias_map[] = {
  sym__expr, 3,
    sym__expr,
    alias_sym_attrib_expr,
    alias_sym_expr,
  sym_string, 2,
    sym_string,
    sym_name,
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 3,
  [8] = 8,
  [9] = 4,
  [10] = 2,
  [11] = 8,
  [12] = 5,
  [13] = 13,
  [14] = 14,
  [15] = 13,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 19,
  [20] = 19,
  [21] = 14,
  [22] = 16,
  [23] = 17,
  [24] = 18,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 26,
  [29] = 29,
  [30] = 27,
  [31] = 31,
  [32] = 29,
  [33] = 31,
  [34] = 34,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 36,
  [39] = 39,
  [40] = 34,
  [41] = 41,
  [42] = 41,
  [43] = 43,
  [44] = 44,
  [45] = 45,
  [46] = 46,
  [47] = 47,
  [48] = 48,
  [49] = 49,
  [50] = 50,
  [51] = 46,
  [52] = 52,
  [53] = 49,
  [54] = 50,
  [55] = 55,
  [56] = 56,
  [57] = 48,
  [58] = 49,
  [59] = 56,
  [60] = 60,
  [61] = 55,
  [62] = 50,
  [63] = 60,
  [64] = 64,
  [65] = 48,
  [66] = 49,
  [67] = 55,
  [68] = 55,
  [69] = 50,
  [70] = 60,
  [71] = 56,
  [72] = 48,
  [73] = 46,
  [74] = 60,
  [75] = 56,
  [76] = 46,
  [77] = 77,
  [78] = 77,
  [79] = 79,
  [80] = 79,
  [81] = 81,
  [82] = 77,
  [83] = 77,
  [84] = 79,
  [85] = 79,
  [86] = 86,
  [87] = 87,
  [88] = 35,
  [89] = 89,
  [90] = 86,
  [91] = 87,
  [92] = 92,
  [93] = 93,
  [94] = 87,
  [95] = 93,
  [96] = 87,
  [97] = 37,
  [98] = 86,
  [99] = 86,
  [100] = 93,
  [101] = 93,
  [102] = 37,
  [103] = 103,
  [104] = 37,
  [105] = 105,
  [106] = 35,
  [107] = 89,
  [108] = 89,
  [109] = 109,
  [110] = 110,
  [111] = 111,
  [112] = 103,
  [113] = 35,
  [114] = 89,
  [115] = 115,
  [116] = 116,
  [117] = 117,
  [118] = 118,
  [119] = 119,
  [120] = 120,
  [121] = 121,
  [122] = 122,
  [123] = 123,
  [124] = 124,
  [125] = 125,
  [126] = 121,
  [127] = 116,
  [128] = 128,
  [129] = 129,
  [130] = 116,
  [131] = 125,
  [132] = 124,
  [133] = 122,
  [134] = 134,
  [135] = 129,
  [136] = 136,
  [137] = 128,
  [138] = 123,
  [139] = 120,
  [140] = 118,
  [141] = 117,
  [142] = 142,
  [143] = 120,
  [144] = 118,
  [145] = 115,
  [146] = 125,
  [147] = 121,
  [148] = 134,
  [149] = 115,
  [150] = 128,
  [151] = 151,
  [152] = 122,
  [153] = 124,
  [154] = 142,
  [155] = 123,
  [156] = 129,
  [157] = 142,
  [158] = 117,
  [159] = 121,
  [160] = 128,
  [161] = 129,
  [162] = 116,
  [163] = 125,
  [164] = 124,
  [165] = 123,
  [166] = 122,
  [167] = 115,
  [168] = 142,
  [169] = 117,
  [170] = 120,
  [171] = 118,
  [172] = 172,
  [173] = 173,
  [174] = 173,
  [175] = 175,
  [176] = 175,
  [177] = 175,
  [178] = 173,
  [179] = 175,
  [180] = 173,
  [181] = 181,
  [182] = 181,
  [183] = 183,
  [184] = 183,
  [185] = 181,
  [186] = 181,
  [187] = 183,
  [188] = 183,
  [189] = 189,
  [190] = 190,
  [191] = 189,
  [192] = 189,
  [193] = 189,
  [194] = 194,
  [195] = 195,
  [196] = 196,
  [197] = 196,
  [198] = 196,
  [199] = 199,
  [200] = 200,
  [201] = 196,
  [202] = 202,
  [203] = 203,
  [204] = 203,
  [205] = 205,
  [206] = 206,
  [207] = 207,
  [208] = 208,
  [209] = 209,
  [210] = 210,
  [211] = 211,
  [212] = 203,
  [213] = 213,
  [214] = 210,
  [215] = 215,
  [216] = 216,
  [217] = 217,
  [218] = 216,
  [219] = 203,
  [220] = 210,
  [221] = 209,
  [222] = 205,
  [223] = 210,
  [224] = 224,
  [225] = 225,
  [226] = 226,
  [227] = 227,
  [228] = 225,
  [229] = 229,
  [230] = 18,
  [231] = 16,
  [232] = 19,
  [233] = 17,
  [234] = 234,
  [235] = 235,
  [236] = 236,
  [237] = 237,
  [238] = 238,
  [239] = 239,
  [240] = 240,
  [241] = 241,
  [242] = 242,
  [243] = 227,
  [244] = 244,
  [245] = 245,
  [246] = 229,
  [247] = 238,
  [248] = 18,
  [249] = 16,
  [250] = 19,
  [251] = 251,
  [252] = 252,
  [253] = 253,
  [254] = 17,
  [255] = 255,
  [256] = 256,
  [257] = 257,
  [258] = 258,
  [259] = 256,
  [260] = 256,
  [261] = 261,
  [262] = 262,
  [263] = 263,
  [264] = 264,
  [265] = 256,
  [266] = 258,
  [267] = 258,
  [268] = 258,
  [269] = 263,
  [270] = 263,
  [271] = 263,
};

static inline bool sym_name_character_set_1(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 931
            ? (c < 748
              ? (c < 192
                ? (c < 170
                  ? (c < 'b'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= 'z')
                  : (c <= 170 || (c < 186
                    ? c == 181
                    : c <= 186)))
                : (c <= 214 || (c < 710
                  ? (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)
                  : (c <= 721 || (c >= 736 && c <= 740)))))
              : (c <= 748 || (c < 895
                ? (c < 886
                  ? (c < 880
                    ? c == 750
                    : c <= 884)
                  : (c <= 887 || (c >= 891 && c <= 893)))
                : (c <= 895 || (c < 908
                  ? (c < 904
                    ? c == 902
                    : c <= 906)
                  : (c <= 908 || (c >= 910 && c <= 929)))))))
            : (c <= 1013 || (c < 1649
              ? (c < 1376
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1015 && c <= 1153)
                    : c <= 1327)
                  : (c <= 1366 || c == 1369))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_name_character_set_2(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 910
            ? (c < 736
              ? (c < 186
                ? (c < 'a'
                  ? (c < '_'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= '_')
                  : (c <= 'z' || (c < 181
                    ? c == 170
                    : c <= 181)))
                : (c <= 186 || (c < 248
                  ? (c < 216
                    ? (c >= 192 && c <= 214)
                    : c <= 246)
                  : (c <= 705 || (c >= 710 && c <= 721)))))
              : (c <= 740 || (c < 891
                ? (c < 880
                  ? (c < 750
                    ? c == 748
                    : c <= 750)
                  : (c <= 884 || (c >= 886 && c <= 887)))
                : (c <= 893 || (c < 904
                  ? (c < 902
                    ? c == 895
                    : c <= 902)
                  : (c <= 906 || c == 908))))))
            : (c <= 929 || (c < 1649
              ? (c < 1376
                ? (c < 1162
                  ? (c < 1015
                    ? (c >= 931 && c <= 1013)
                    : c <= 1153)
                  : (c <= 1327 || (c < 1369
                    ? (c >= 1329 && c <= 1366)
                    : c <= 1369)))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_name_character_set_3(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 910
            ? (c < 736
              ? (c < 186
                ? (c < 'b'
                  ? (c < '_'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= '_')
                  : (c <= 'z' || (c < 181
                    ? c == 170
                    : c <= 181)))
                : (c <= 186 || (c < 248
                  ? (c < 216
                    ? (c >= 192 && c <= 214)
                    : c <= 246)
                  : (c <= 705 || (c >= 710 && c <= 721)))))
              : (c <= 740 || (c < 891
                ? (c < 880
                  ? (c < 750
                    ? c == 748
                    : c <= 750)
                  : (c <= 884 || (c >= 886 && c <= 887)))
                : (c <= 893 || (c < 904
                  ? (c < 902
                    ? c == 895
                    : c <= 902)
                  : (c <= 906 || c == 908))))))
            : (c <= 929 || (c < 1649
              ? (c < 1376
                ? (c < 1162
                  ? (c < 1015
                    ? (c >= 931 && c <= 1013)
                    : c <= 1153)
                  : (c <= 1327 || (c < 1369
                    ? (c >= 1329 && c <= 1366)
                    : c <= 1369)))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_name_character_set_4(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'a' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static inline bool sym_name_character_set_5(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'B'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'a' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static inline bool sym_name_character_set_6(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'b' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static inline bool sym_name_character_set_7(int32_t c) {
  return (c < 43642
    ? (c < 3792
      ? (c < 2763
        ? (c < 2112
          ? (c < 1162
            ? (c < 748
              ? (c < 186
                ? (c < 170
                  ? (c < 'a'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= 'z')
                  : (c <= 170 || (c < 183
                    ? c == 181
                    : c <= 183)))
                : (c <= 186 || (c < 248
                  ? (c < 216
                    ? (c >= 192 && c <= 214)
                    : c <= 246)
                  : (c <= 705 || (c < 736
                    ? (c >= 710 && c <= 721)
                    : c <= 740)))))
              : (c <= 748 || (c < 902
                ? (c < 886
                  ? (c < 768
                    ? c == 750
                    : c <= 884)
                  : (c <= 887 || (c < 895
                    ? (c >= 891 && c <= 893)
                    : c <= 895)))
                : (c <= 906 || (c < 931
                  ? (c < 910
                    ? c == 908
                    : c <= 929)
                  : (c <= 1013 || (c < 1155
                    ? (c >= 1015 && c <= 1153)
                    : c <= 1159)))))))
            : (c <= 1327 || (c < 1568
              ? (c < 1473
                ? (c < 1376
                  ? (c < 1369
                    ? (c >= 1329 && c <= 1366)
                    : c <= 1369)
                  : (c <= 1416 || (c < 1471
                    ? (c >= 1425 && c <= 1469)
                    : c <= 1471)))
                : (c <= 1474 || (c < 1488
                  ? (c < 1479
                    ? (c >= 1476 && c <= 1477)
                    : c <= 1479)
                  : (c <= 1514 || (c < 1552
                    ? (c >= 1519 && c <= 1522)
                    : c <= 1562)))))
              : (c <= 1641 || (c < 1808
                ? (c < 1759
                  ? (c < 1749
                    ? (c >= 1646 && c <= 1747)
                    : c <= 1756)
                  : (c <= 1768 || (c < 1791
                    ? (c >= 1770 && c <= 1788)
                    : c <= 1791)))
                : (c <= 1866 || (c < 2042
                  ? (c < 1984
                    ? (c >= 1869 && c <= 1969)
                    : c <= 2037)
                  : (c <= 2042 || (c < 2048
                    ? c == 2045
                    : c <= 2093)))))))))
          : (c <= 2139 || (c < 2565
            ? (c < 2482
              ? (c < 2406
                ? (c < 2185
                  ? (c < 2160
                    ? (c >= 2144 && c <= 2154)
                    : c <= 2183)
                  : (c <= 2190 || (c < 2275
                    ? (c >= 2200 && c <= 2273)
                    : c <= 2403)))
                : (c <= 2415 || (c < 2447
                  ? (c < 2437
                    ? (c >= 2417 && c <= 2435)
                    : c <= 2444)
                  : (c <= 2448 || (c < 2474
                    ? (c >= 2451 && c <= 2472)
                    : c <= 2480)))))
              : (c <= 2482 || (c < 2524
                ? (c < 2503
                  ? (c < 2492
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2500)
                  : (c <= 2504 || (c < 2519
                    ? (c >= 2507 && c <= 2510)
                    : c <= 2519)))
                : (c <= 2525 || (c < 2556
                  ? (c < 2534
                    ? (c >= 2527 && c <= 2531)
                    : c <= 2545)
                  : (c <= 2556 || (c < 2561
                    ? c == 2558
                    : c <= 2563)))))))
            : (c <= 2570 || (c < 2649
              ? (c < 2616
                ? (c < 2602
                  ? (c < 2579
                    ? (c >= 2575 && c <= 2576)
                    : c <= 2600)
                  : (c <= 2608 || (c < 2613
                    ? (c >= 2610 && c <= 2611)
                    : c <= 2614)))
                : (c <= 2617 || (c < 2631
                  ? (c < 2622
                    ? c == 2620
                    : c <= 2626)
                  : (c <= 2632 || (c < 2641
                    ? (c >= 2635 && c <= 2637)
                    : c <= 2641)))))
              : (c <= 2652 || (c < 2707
                ? (c < 2689
                  ? (c < 2662
                    ? c == 2654
                    : c <= 2677)
                  : (c <= 2691 || (c < 2703
                    ? (c >= 2693 && c <= 2701)
                    : c <= 2705)))
                : (c <= 2728 || (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2759
                    ? (c >= 2748 && c <= 2757)
                    : c <= 2761)))))))))))
        : (c <= 2765 || (c < 3200
          ? (c < 2969
            ? (c < 2876
              ? (c < 2821
                ? (c < 2790
                  ? (c < 2784
                    ? c == 2768
                    : c <= 2787)
                  : (c <= 2799 || (c < 2817
                    ? (c >= 2809 && c <= 2815)
                    : c <= 2819)))
                : (c <= 2828 || (c < 2858
                  ? (c < 2835
                    ? (c >= 2831 && c <= 2832)
                    : c <= 2856)
                  : (c <= 2864 || (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)))))
              : (c <= 2884 || (c < 2918
                ? (c < 2901
                  ? (c < 2891
                    ? (c >= 2887 && c <= 2888)
                    : c <= 2893)
                  : (c <= 2903 || (c < 2911
                    ? (c >= 2908 && c <= 2909)
                    : c <= 2915)))
                : (c <= 2927 || (c < 2949
                  ? (c < 2946
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c < 2962
                    ? (c >= 2958 && c <= 2960)
                    : c <= 2965)))))))
            : (c <= 2970 || (c < 3072
              ? (c < 3006
                ? (c < 2979
                  ? (c < 2974
                    ? c == 2972
                    : c <= 2975)
                  : (c <= 2980 || (c < 2990
                    ? (c >= 2984 && c <= 2986)
                    : c <= 3001)))
                : (c <= 3010 || (c < 3024
                  ? (c < 3018
                    ? (c >= 3014 && c <= 3016)
                    : c <= 3021)
                  : (c <= 3024 || (c < 3046
                    ? c == 3031
                    : c <= 3055)))))
              : (c <= 3084 || (c < 3146
                ? (c < 3114
                  ? (c < 3090
                    ? (c >= 3086 && c <= 3088)
                    : c <= 3112)
                  : (c <= 3129 || (c < 3142
                    ? (c >= 3132 && c <= 3140)
                    : c <= 3144)))
                : (c <= 3149 || (c < 3165
                  ? (c < 3160
                    ? (c >= 3157 && c <= 3158)
                    : c <= 3162)
                  : (c <= 3165 || (c < 3174
                    ? (c >= 3168 && c <= 3171)
                    : c <= 3183)))))))))
          : (c <= 3203 || (c < 3461
            ? (c < 3302
              ? (c < 3260
                ? (c < 3218
                  ? (c < 3214
                    ? (c >= 3205 && c <= 3212)
                    : c <= 3216)
                  : (c <= 3240 || (c < 3253
                    ? (c >= 3242 && c <= 3251)
                    : c <= 3257)))
                : (c <= 3268 || (c < 3285
                  ? (c < 3274
                    ? (c >= 3270 && c <= 3272)
                    : c <= 3277)
                  : (c <= 3286 || (c < 3296
                    ? (c >= 3293 && c <= 3294)
                    : c <= 3299)))))
              : (c <= 3311 || (c < 3402
                ? (c < 3342
                  ? (c < 3328
                    ? (c >= 3313 && c <= 3314)
                    : c <= 3340)
                  : (c <= 3344 || (c < 3398
                    ? (c >= 3346 && c <= 3396)
                    : c <= 3400)))
                : (c <= 3406 || (c < 3430
                  ? (c < 3423
                    ? (c >= 3412 && c <= 3415)
                    : c <= 3427)
                  : (c <= 3439 || (c < 3457
                    ? (c >= 3450 && c <= 3455)
                    : c <= 3459)))))))
            : (c <= 3478 || (c < 3648
              ? (c < 3535
                ? (c < 3517
                  ? (c < 3507
                    ? (c >= 3482 && c <= 3505)
                    : c <= 3515)
                  : (c <= 3517 || (c < 3530
                    ? (c >= 3520 && c <= 3526)
                    : c <= 3530)))
                : (c <= 3540 || (c < 3558
                  ? (c < 3544
                    ? c == 3542
                    : c <= 3551)
                  : (c <= 3567 || (c < 3585
                    ? (c >= 3570 && c <= 3571)
                    : c <= 3642)))))
              : (c <= 3662 || (c < 3749
                ? (c < 3716
                  ? (c < 3713
                    ? (c >= 3664 && c <= 3673)
                    : c <= 3714)
                  : (c <= 3716 || (c < 3724
                    ? (c >= 3718 && c <= 3722)
                    : c <= 3747)))
                : (c <= 3749 || (c < 3782
                  ? (c < 3776
                    ? (c >= 3751 && c <= 3773)
                    : c <= 3780)
                  : (c <= 3782 || (c >= 3784 && c <= 3789)))))))))))))
      : (c <= 3801 || (c < 8027
        ? (c < 5952
          ? (c < 4698
            ? (c < 3993
              ? (c < 3895
                ? (c < 3864
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3865 || (c < 3893
                    ? (c >= 3872 && c <= 3881)
                    : c <= 3893)))
                : (c <= 3895 || (c < 3913
                  ? (c < 3902
                    ? c == 3897
                    : c <= 3911)
                  : (c <= 3948 || (c < 3974
                    ? (c >= 3953 && c <= 3972)
                    : c <= 3991)))))
              : (c <= 4028 || (c < 4301
                ? (c < 4176
                  ? (c < 4096
                    ? c == 4038
                    : c <= 4169)
                  : (c <= 4253 || (c < 4295
                    ? (c >= 4256 && c <= 4293)
                    : c <= 4295)))
                : (c <= 4301 || (c < 4682
                  ? (c < 4348
                    ? (c >= 4304 && c <= 4346)
                    : c <= 4680)
                  : (c <= 4685 || (c < 4696
                    ? (c >= 4688 && c <= 4694)
                    : c <= 4696)))))))
            : (c <= 4701 || (c < 4957
              ? (c < 4800
                ? (c < 4752
                  ? (c < 4746
                    ? (c >= 4704 && c <= 4744)
                    : c <= 4749)
                  : (c <= 4784 || (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)))
                : (c <= 4800 || (c < 4824
                  ? (c < 4808
                    ? (c >= 4802 && c <= 4805)
                    : c <= 4822)
                  : (c <= 4880 || (c < 4888
                    ? (c >= 4882 && c <= 4885)
                    : c <= 4954)))))
              : (c <= 4959 || (c < 5743
                ? (c < 5024
                  ? (c < 4992
                    ? (c >= 4969 && c <= 4977)
                    : c <= 5007)
                  : (c <= 5109 || (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)))
                : (c <= 5759 || (c < 5870
                  ? (c < 5792
                    ? (c >= 5761 && c <= 5786)
                    : c <= 5866)
                  : (c <= 5880 || (c < 5919
                    ? (c >= 5888 && c <= 5909)
                    : c <= 5940)))))))))
          : (c <= 5971 || (c < 6783
            ? (c < 6320
              ? (c < 6108
                ? (c < 6002
                  ? (c < 5998
                    ? (c >= 5984 && c <= 5996)
                    : c <= 6000)
                  : (c <= 6003 || (c < 6103
                    ? (c >= 6016 && c <= 6099)
                    : c <= 6103)))
                : (c <= 6109 || (c < 6159
                  ? (c < 6155
                    ? (c >= 6112 && c <= 6121)
                    : c <= 6157)
                  : (c <= 6169 || (c < 6272
                    ? (c >= 6176 && c <= 6264)
                    : c <= 6314)))))
              : (c <= 6389 || (c < 6528
                ? (c < 6448
                  ? (c < 6432
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6443)
                  : (c <= 6459 || (c < 6512
                    ? (c >= 6470 && c <= 6509)
                    : c <= 6516)))
                : (c <= 6571 || (c < 6656
                  ? (c < 6608
                    ? (c >= 6576 && c <= 6601)
                    : c <= 6618)
                  : (c <= 6683 || (c < 6752
                    ? (c >= 6688 && c <= 6750)
                    : c <= 6780)))))))
            : (c <= 6793 || (c < 7296
              ? (c < 6992
                ? (c < 6832
                  ? (c < 6823
                    ? (c >= 6800 && c <= 6809)
                    : c <= 6823)
                  : (c <= 6845 || (c < 6912
                    ? (c >= 6847 && c <= 6862)
                    : c <= 6988)))
                : (c <= 7001 || (c < 7168
                  ? (c < 7040
                    ? (c >= 7019 && c <= 7027)
                    : c <= 7155)
                  : (c <= 7223 || (c < 7245
                    ? (c >= 7232 && c <= 7241)
                    : c <= 7293)))))
              : (c <= 7304 || (c < 7960
                ? (c < 7376
                  ? (c < 7357
                    ? (c >= 7312 && c <= 7354)
                    : c <= 7359)
                  : (c <= 7378 || (c < 7424
                    ? (c >= 7380 && c <= 7418)
                    : c <= 7957)))
                : (c <= 7965 || (c < 8016
                  ? (c < 8008
                    ? (c >= 7968 && c <= 8005)
                    : c <= 8013)
                  : (c <= 8023 || c == 8025))))))))))
        : (c <= 8027 || (c < 11728
          ? (c < 8469
            ? (c < 8182
              ? (c < 8130
                ? (c < 8064
                  ? (c < 8031
                    ? c == 8029
                    : c <= 8061)
                  : (c <= 8116 || (c < 8126
                    ? (c >= 8118 && c <= 8124)
                    : c <= 8126)))
                : (c <= 8132 || (c < 8150
                  ? (c < 8144
                    ? (c >= 8134 && c <= 8140)
                    : c <= 8147)
                  : (c <= 8155 || (c < 8178
                    ? (c >= 8160 && c <= 8172)
                    : c <= 8180)))))
              : (c <= 8188 || (c < 8400
                ? (c < 8305
                  ? (c < 8276
                    ? (c >= 8255 && c <= 8256)
                    : c <= 8276)
                  : (c <= 8305 || (c < 8336
                    ? c == 8319
                    : c <= 8348)))
                : (c <= 8412 || (c < 8450
                  ? (c < 8421
                    ? c == 8417
                    : c <= 8432)
                  : (c <= 8450 || (c < 8458
                    ? c == 8455
                    : c <= 8467)))))))
            : (c <= 8469 || (c < 11520
              ? (c < 8508
                ? (c < 8486
                  ? (c < 8484
                    ? (c >= 8472 && c <= 8477)
                    : c <= 8484)
                  : (c <= 8486 || (c < 8490
                    ? c == 8488
                    : c <= 8505)))
                : (c <= 8511 || (c < 8544
                  ? (c < 8526
                    ? (c >= 8517 && c <= 8521)
                    : c <= 8526)
                  : (c <= 8584 || (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11507)))))
              : (c <= 11557 || (c < 11680
                ? (c < 11568
                  ? (c < 11565
                    ? c == 11559
                    : c <= 11565)
                  : (c <= 11623 || (c < 11647
                    ? c == 11631
                    : c <= 11670)))
                : (c <= 11686 || (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c < 11720
                    ? (c >= 11712 && c <= 11718)
                    : c <= 11726)))))))))
          : (c <= 11734 || (c < 42775
            ? (c < 12549
              ? (c < 12344
                ? (c < 12293
                  ? (c < 11744
                    ? (c >= 11736 && c <= 11742)
                    : c <= 11775)
                  : (c <= 12295 || (c < 12337
                    ? (c >= 12321 && c <= 12335)
                    : c <= 12341)))
                : (c <= 12348 || (c < 12445
                  ? (c < 12441
                    ? (c >= 12353 && c <= 12438)
                    : c <= 12442)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))))
              : (c <= 12591 || (c < 42192
                ? (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c < 19968
                    ? (c >= 13312 && c <= 19903)
                    : c <= 42124)))
                : (c <= 42237 || (c < 42560
                  ? (c < 42512
                    ? (c >= 42240 && c <= 42508)
                    : c <= 42539)
                  : (c <= 42607 || (c < 42623
                    ? (c >= 42612 && c <= 42621)
                    : c <= 42737)))))))
            : (c <= 42783 || (c < 43259
              ? (c < 42994
                ? (c < 42960
                  ? (c < 42891
                    ? (c >= 42786 && c <= 42888)
                    : c <= 42954)
                  : (c <= 42961 || (c < 42965
                    ? c == 42963
                    : c <= 42969)))
                : (c <= 43047 || (c < 43136
                  ? (c < 43072
                    ? c == 43052
                    : c <= 43123)
                  : (c <= 43205 || (c < 43232
                    ? (c >= 43216 && c <= 43225)
                    : c <= 43255)))))
              : (c <= 43259 || (c < 43488
                ? (c < 43360
                  ? (c < 43312
                    ? (c >= 43261 && c <= 43309)
                    : c <= 43347)
                  : (c <= 43388 || (c < 43471
                    ? (c >= 43392 && c <= 43456)
                    : c <= 43481)))
                : (c <= 43518 || (c < 43600
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43574)
                    : c <= 43597)
                  : (c <= 43609 || (c >= 43616 && c <= 43638)))))))))))))))
    : (c <= 43714 || (c < 71472
      ? (c < 67644
        ? (c < 65382
          ? (c < 64318
            ? (c < 44012
              ? (c < 43793
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43759)
                  : (c <= 43766 || (c < 43785
                    ? (c >= 43777 && c <= 43782)
                    : c <= 43790)))
                : (c <= 43798 || (c < 43824
                  ? (c < 43816
                    ? (c >= 43808 && c <= 43814)
                    : c <= 43822)
                  : (c <= 43866 || (c < 43888
                    ? (c >= 43868 && c <= 43881)
                    : c <= 44010)))))
              : (c <= 44013 || (c < 64112
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 44016 && c <= 44025)
                    : c <= 55203)
                  : (c <= 55238 || (c < 63744
                    ? (c >= 55243 && c <= 55291)
                    : c <= 64109)))
                : (c <= 64217 || (c < 64285
                  ? (c < 64275
                    ? (c >= 64256 && c <= 64262)
                    : c <= 64279)
                  : (c <= 64296 || (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)))))))
            : (c <= 64318 || (c < 65101
              ? (c < 64848
                ? (c < 64326
                  ? (c < 64323
                    ? (c >= 64320 && c <= 64321)
                    : c <= 64324)
                  : (c <= 64433 || (c < 64612
                    ? (c >= 64467 && c <= 64605)
                    : c <= 64829)))
                : (c <= 64911 || (c < 65024
                  ? (c < 65008
                    ? (c >= 64914 && c <= 64967)
                    : c <= 65017)
                  : (c <= 65039 || (c < 65075
                    ? (c >= 65056 && c <= 65071)
                    : c <= 65076)))))
              : (c <= 65103 || (c < 65149
                ? (c < 65143
                  ? (c < 65139
                    ? c == 65137
                    : c <= 65139)
                  : (c <= 65143 || (c < 65147
                    ? c == 65145
                    : c <= 65147)))
                : (c <= 65149 || (c < 65313
                  ? (c < 65296
                    ? (c >= 65151 && c <= 65276)
                    : c <= 65305)
                  : (c <= 65338 || (c < 65345
                    ? c == 65343
                    : c <= 65370)))))))))
          : (c <= 65470 || (c < 66560
            ? (c < 65856
              ? (c < 65549
                ? (c < 65490
                  ? (c < 65482
                    ? (c >= 65474 && c <= 65479)
                    : c <= 65487)
                  : (c <= 65495 || (c < 65536
                    ? (c >= 65498 && c <= 65500)
                    : c <= 65547)))
                : (c <= 65574 || (c < 65599
                  ? (c < 65596
                    ? (c >= 65576 && c <= 65594)
                    : c <= 65597)
                  : (c <= 65613 || (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)))))
              : (c <= 65908 || (c < 66349
                ? (c < 66208
                  ? (c < 66176
                    ? c == 66045
                    : c <= 66204)
                  : (c <= 66256 || (c < 66304
                    ? c == 66272
                    : c <= 66335)))
                : (c <= 66378 || (c < 66464
                  ? (c < 66432
                    ? (c >= 66384 && c <= 66426)
                    : c <= 66461)
                  : (c <= 66499 || (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)))))))
            : (c <= 66717 || (c < 66995
              ? (c < 66928
                ? (c < 66776
                  ? (c < 66736
                    ? (c >= 66720 && c <= 66729)
                    : c <= 66771)
                  : (c <= 66811 || (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)))
                : (c <= 66938 || (c < 66964
                  ? (c < 66956
                    ? (c >= 66940 && c <= 66954)
                    : c <= 66962)
                  : (c <= 66965 || (c < 66979
                    ? (c >= 66967 && c <= 66977)
                    : c <= 66993)))))
              : (c <= 67001 || (c < 67463
                ? (c < 67392
                  ? (c < 67072
                    ? (c >= 67003 && c <= 67004)
                    : c <= 67382)
                  : (c <= 67413 || (c < 67456
                    ? (c >= 67424 && c <= 67431)
                    : c <= 67461)))
                : (c <= 67504 || (c < 67592
                  ? (c < 67584
                    ? (c >= 67506 && c <= 67514)
                    : c <= 67589)
                  : (c <= 67592 || (c < 67639
                    ? (c >= 67594 && c <= 67637)
                    : c <= 67640)))))))))))
        : (c <= 67644 || (c < 69968
          ? (c < 68480
            ? (c < 68108
              ? (c < 67840
                ? (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c < 67828
                    ? (c >= 67808 && c <= 67826)
                    : c <= 67829)))
                : (c <= 67861 || (c < 68030
                  ? (c < 67968
                    ? (c >= 67872 && c <= 67897)
                    : c <= 68023)
                  : (c <= 68031 || (c < 68101
                    ? (c >= 68096 && c <= 68099)
                    : c <= 68102)))))
              : (c <= 68115 || (c < 68224
                ? (c < 68152
                  ? (c < 68121
                    ? (c >= 68117 && c <= 68119)
                    : c <= 68149)
                  : (c <= 68154 || (c < 68192
                    ? c == 68159
                    : c <= 68220)))
                : (c <= 68252 || (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68326)
                  : (c <= 68405 || (c < 68448
                    ? (c >= 68416 && c <= 68437)
                    : c <= 68466)))))))
            : (c <= 68497 || (c < 69488
              ? (c < 69248
                ? (c < 68800
                  ? (c < 68736
                    ? (c >= 68608 && c <= 68680)
                    : c <= 68786)
                  : (c <= 68850 || (c < 68912
                    ? (c >= 68864 && c <= 68903)
                    : c <= 68921)))
                : (c <= 69289 || (c < 69376
                  ? (c < 69296
                    ? (c >= 69291 && c <= 69292)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69456)))))
              : (c <= 69509 || (c < 69826
                ? (c < 69632
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69702 || (c < 69759
                    ? (c >= 69734 && c <= 69749)
                    : c <= 69818)))
                : (c <= 69826 || (c < 69888
                  ? (c < 69872
                    ? (c >= 69840 && c <= 69864)
                    : c <= 69881)
                  : (c <= 69940 || (c < 69956
                    ? (c >= 69942 && c <= 69951)
                    : c <= 69959)))))))))
          : (c <= 70003 || (c < 70471
            ? (c < 70287
              ? (c < 70144
                ? (c < 70089
                  ? (c < 70016
                    ? c == 70006
                    : c <= 70084)
                  : (c <= 70092 || (c < 70108
                    ? (c >= 70094 && c <= 70106)
                    : c <= 70108)))
                : (c <= 70161 || (c < 70272
                  ? (c < 70206
                    ? (c >= 70163 && c <= 70199)
                    : c <= 70206)
                  : (c <= 70278 || (c < 70282
                    ? c == 70280
                    : c <= 70285)))))
              : (c <= 70301 || (c < 70415
                ? (c < 70384
                  ? (c < 70320
                    ? (c >= 70303 && c <= 70312)
                    : c <= 70378)
                  : (c <= 70393 || (c < 70405
                    ? (c >= 70400 && c <= 70403)
                    : c <= 70412)))
                : (c <= 70416 || (c < 70450
                  ? (c < 70442
                    ? (c >= 70419 && c <= 70440)
                    : c <= 70448)
                  : (c <= 70451 || (c < 70459
                    ? (c >= 70453 && c <= 70457)
                    : c <= 70468)))))))
            : (c <= 70472 || (c < 70864
              ? (c < 70512
                ? (c < 70487
                  ? (c < 70480
                    ? (c >= 70475 && c <= 70477)
                    : c <= 70480)
                  : (c <= 70487 || (c < 70502
                    ? (c >= 70493 && c <= 70499)
                    : c <= 70508)))
                : (c <= 70516 || (c < 70750
                  ? (c < 70736
                    ? (c >= 70656 && c <= 70730)
                    : c <= 70745)
                  : (c <= 70753 || (c < 70855
                    ? (c >= 70784 && c <= 70853)
                    : c <= 70855)))))
              : (c <= 70873 || (c < 71248
                ? (c < 71128
                  ? (c < 71096
                    ? (c >= 71040 && c <= 71093)
                    : c <= 71104)
                  : (c <= 71133 || (c < 71236
                    ? (c >= 71168 && c <= 71232)
                    : c <= 71236)))
                : (c <= 71257 || (c < 71424
                  ? (c < 71360
                    ? (c >= 71296 && c <= 71352)
                    : c <= 71369)
                  : (c <= 71450 || (c >= 71453 && c <= 71467)))))))))))))
      : (c <= 71481 || (c < 119973
        ? (c < 82944
          ? (c < 72784
            ? (c < 72096
              ? (c < 71948
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71738)
                  : (c <= 71913 || (c < 71945
                    ? (c >= 71935 && c <= 71942)
                    : c <= 71945)))
                : (c <= 71955 || (c < 71991
                  ? (c < 71960
                    ? (c >= 71957 && c <= 71958)
                    : c <= 71989)
                  : (c <= 71992 || (c < 72016
                    ? (c >= 71995 && c <= 72003)
                    : c <= 72025)))))
              : (c <= 72103 || (c < 72272
                ? (c < 72163
                  ? (c < 72154
                    ? (c >= 72106 && c <= 72151)
                    : c <= 72161)
                  : (c <= 72164 || (c < 72263
                    ? (c >= 72192 && c <= 72254)
                    : c <= 72263)))
                : (c <= 72345 || (c < 72704
                  ? (c < 72368
                    ? c == 72349
                    : c <= 72440)
                  : (c <= 72712 || (c < 72760
                    ? (c >= 72714 && c <= 72758)
                    : c <= 72768)))))))
            : (c <= 72793 || (c < 73063
              ? (c < 72971
                ? (c < 72873
                  ? (c < 72850
                    ? (c >= 72818 && c <= 72847)
                    : c <= 72871)
                  : (c <= 72886 || (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)))
                : (c <= 73014 || (c < 73023
                  ? (c < 73020
                    ? c == 73018
                    : c <= 73021)
                  : (c <= 73031 || (c < 73056
                    ? (c >= 73040 && c <= 73049)
                    : c <= 73061)))))
              : (c <= 73064 || (c < 73648
                ? (c < 73107
                  ? (c < 73104
                    ? (c >= 73066 && c <= 73102)
                    : c <= 73105)
                  : (c <= 73112 || (c < 73440
                    ? (c >= 73120 && c <= 73129)
                    : c <= 73462)))
                : (c <= 73648 || (c < 74880
                  ? (c < 74752
                    ? (c >= 73728 && c <= 74649)
                    : c <= 74862)
                  : (c <= 75075 || (c < 77824
                    ? (c >= 77712 && c <= 77808)
                    : c <= 78894)))))))))
          : (c <= 83526 || (c < 110581
            ? (c < 93053
              ? (c < 92880
                ? (c < 92768
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92777 || (c < 92864
                    ? (c >= 92784 && c <= 92862)
                    : c <= 92873)))
                : (c <= 92909 || (c < 92992
                  ? (c < 92928
                    ? (c >= 92912 && c <= 92916)
                    : c <= 92982)
                  : (c <= 92995 || (c < 93027
                    ? (c >= 93008 && c <= 93017)
                    : c <= 93047)))))
              : (c <= 93071 || (c < 94179
                ? (c < 94031
                  ? (c < 93952
                    ? (c >= 93760 && c <= 93823)
                    : c <= 94026)
                  : (c <= 94087 || (c < 94176
                    ? (c >= 94095 && c <= 94111)
                    : c <= 94177)))
                : (c <= 94180 || (c < 100352
                  ? (c < 94208
                    ? (c >= 94192 && c <= 94193)
                    : c <= 100343)
                  : (c <= 101589 || (c < 110576
                    ? (c >= 101632 && c <= 101640)
                    : c <= 110579)))))))
            : (c <= 110587 || (c < 118576
              ? (c < 113664
                ? (c < 110928
                  ? (c < 110592
                    ? (c >= 110589 && c <= 110590)
                    : c <= 110882)
                  : (c <= 110930 || (c < 110960
                    ? (c >= 110948 && c <= 110951)
                    : c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c < 118528
                    ? (c >= 113821 && c <= 113822)
                    : c <= 118573)))))
              : (c <= 118598 || (c < 119362
                ? (c < 119163
                  ? (c < 119149
                    ? (c >= 119141 && c <= 119145)
                    : c <= 119154)
                  : (c <= 119170 || (c < 119210
                    ? (c >= 119173 && c <= 119179)
                    : c <= 119213)))
                : (c <= 119364 || (c < 119966
                  ? (c < 119894
                    ? (c >= 119808 && c <= 119892)
                    : c <= 119964)
                  : (c <= 119967 || c == 119970))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(61);
      if (lookahead == '!') ADVANCE(73);
      if (lookahead == '"') ADVANCE(82);
      if (lookahead == '&') ADVANCE(70);
      if (lookahead == '(') ADVANCE(80);
      if (lookahead == ')') ADVANCE(81);
      if (lookahead == '*') ADVANCE(67);
      if (lookahead == '+') ADVANCE(77);
      if (lookahead == ',') ADVANCE(63);
      if (lookahead == '-') ADVANCE(78);
      if (lookahead == '.') ADVANCE(106);
      if (lookahead == '/') ADVANCE(79);
      if (lookahead == '0') ADVANCE(90);
      if (lookahead == '<') ADVANCE(75);
      if (lookahead == '=') ADVANCE(76);
      if (lookahead == '>') ADVANCE(74);
      if (lookahead == 'S') ADVANCE(117);
      if (lookahead == '[') ADVANCE(65);
      if (lookahead == '\\') SKIP(57)
      if (lookahead == ']') ADVANCE(68);
      if (lookahead == '_') ADVANCE(138);
      if (lookahead == 'a') ADVANCE(126);
      if (lookahead == 'f') ADVANCE(122);
      if (lookahead == 'g') ADVANCE(131);
      if (lookahead == '{') ADVANCE(62);
      if (lookahead == '|') ADVANCE(69);
      if (lookahead == '}') ADVANCE(64);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ' ||
          lookahead == 8203 ||
          lookahead == 8288 ||
          lookahead == 65279) SKIP(0)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(91);
      if (sym_name_character_set_1(lookahead)) ADVANCE(139);
      END_STATE();
    case 1:
      if (lookahead == '\n') SKIP(5)
      END_STATE();
    case 2:
      if (lookahead == '\n') SKIP(5)
      if (lookahead == '\r') SKIP(1)
      END_STATE();
    case 3:
      if (lookahead == '\n') SKIP(6)
      END_STATE();
    case 4:
      if (lookahead == '\n') SKIP(6)
      if (lookahead == '\r') SKIP(3)
      END_STATE();
    case 5:
      if (lookahead == '!') ADVANCE(73);
      if (lookahead == '"') ADVANCE(82);
      if (lookahead == '&') ADVANCE(70);
      if (lookahead == '(') ADVANCE(80);
      if (lookahead == ')') ADVANCE(81);
      if (lookahead == '*') ADVANCE(67);
      if (lookahead == '+') ADVANCE(77);
      if (lookahead == ',') ADVANCE(63);
      if (lookahead == '-') ADVANCE(78);
      if (lookahead == '.') ADVANCE(105);
      if (lookahead == '/') ADVANCE(79);
      if (lookahead == '0') ADVANCE(90);
      if (lookahead == '<') ADVANCE(75);
      if (lookahead == '=') ADVANCE(76);
      if (lookahead == '>') ADVANCE(74);
      if (lookahead == '[') ADVANCE(65);
      if (lookahead == '\\') SKIP(2)
      if (lookahead == ']') ADVANCE(68);
      if (lookahead == '{') ADVANCE(62);
      if (lookahead == '|') ADVANCE(69);
      if (lookahead == '}') ADVANCE(64);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ' ||
          lookahead == 8203 ||
          lookahead == 8288 ||
          lookahead == 65279) SKIP(5)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(91);
      if (sym_name_character_set_2(lookahead)) ADVANCE(139);
      END_STATE();
    case 6:
      if (lookahead == '*') ADVANCE(67);
      if (lookahead == '.') ADVANCE(10);
      if (lookahead == '/') ADVANCE(7);
      if (lookahead == '\\') SKIP(4)
      if (lookahead == ']') ADVANCE(68);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ' ||
          lookahead == 8203 ||
          lookahead == 8288 ||
          lookahead == 65279) SKIP(6)
      if (('1' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(140);
      END_STATE();
    case 7:
      if (lookahead == '*') ADVANCE(9);
      if (lookahead == '/') ADVANCE(103);
      END_STATE();
    case 8:
      if (lookahead == '*') ADVANCE(8);
      if (lookahead == '/') ADVANCE(102);
      if (lookahead != 0) ADVANCE(9);
      END_STATE();
    case 9:
      if (lookahead == '*') ADVANCE(8);
      if (lookahead != 0) ADVANCE(9);
      END_STATE();
    case 10:
      if (lookahead == '.') ADVANCE(66);
      END_STATE();
    case 11:
      if (lookahead == '>') ADVANCE(72);
      END_STATE();
    case 12:
      if (lookahead == 'a') ADVANCE(42);
      END_STATE();
    case 13:
      if (lookahead == 'a') ADVANCE(34);
      END_STATE();
    case 14:
      if (lookahead == 'a') ADVANCE(35);
      END_STATE();
    case 15:
      if (lookahead == 'a') ADVANCE(43);
      END_STATE();
    case 16:
      if (lookahead == 'c') ADVANCE(12);
      END_STATE();
    case 17:
      if (lookahead == 'c') ADVANCE(46);
      END_STATE();
    case 18:
      if (lookahead == 'c') ADVANCE(15);
      END_STATE();
    case 19:
      if (lookahead == 'd') ADVANCE(26);
      END_STATE();
    case 20:
      if (lookahead == 'd') ADVANCE(30);
      END_STATE();
    case 21:
      if (lookahead == 'e') ADVANCE(48);
      END_STATE();
    case 22:
      if (lookahead == 'e') ADVANCE(32);
      END_STATE();
    case 23:
      if (lookahead == 'e') ADVANCE(33);
      END_STATE();
    case 24:
      if (lookahead == 'e') ADVANCE(49);
      END_STATE();
    case 25:
      if (lookahead == 'f') ADVANCE(47);
      END_STATE();
    case 26:
      if (lookahead == 'i') ADVANCE(39);
      END_STATE();
    case 27:
      if (lookahead == 'i') ADVANCE(41);
      END_STATE();
    case 28:
      if (lookahead == 'i') ADVANCE(44);
      END_STATE();
    case 29:
      if (lookahead == 'i') ADVANCE(45);
      END_STATE();
    case 30:
      if (lookahead == 'i') ADVANCE(40);
      END_STATE();
    case 31:
      if (lookahead == 'l') ADVANCE(21);
      END_STATE();
    case 32:
      if (lookahead == 'l') ADVANCE(108);
      END_STATE();
    case 33:
      if (lookahead == 'l') ADVANCE(107);
      END_STATE();
    case 34:
      if (lookahead == 'l') ADVANCE(28);
      END_STATE();
    case 35:
      if (lookahead == 'l') ADVANCE(29);
      END_STATE();
    case 36:
      if (lookahead == 'l') ADVANCE(24);
      END_STATE();
    case 37:
      if (lookahead == 'n') ADVANCE(111);
      END_STATE();
    case 38:
      if (lookahead == 'n') ADVANCE(17);
      END_STATE();
    case 39:
      if (lookahead == 'n') ADVANCE(13);
      END_STATE();
    case 40:
      if (lookahead == 'n') ADVANCE(14);
      END_STATE();
    case 41:
      if (lookahead == 'o') ADVANCE(37);
      END_STATE();
    case 42:
      if (lookahead == 'r') ADVANCE(19);
      END_STATE();
    case 43:
      if (lookahead == 'r') ADVANCE(20);
      END_STATE();
    case 44:
      if (lookahead == 't') ADVANCE(50);
      END_STATE();
    case 45:
      if (lookahead == 't') ADVANCE(51);
      END_STATE();
    case 46:
      if (lookahead == 't') ADVANCE(27);
      END_STATE();
    case 47:
      if (lookahead == 'u') ADVANCE(38);
      END_STATE();
    case 48:
      if (lookahead == 'v') ADVANCE(22);
      END_STATE();
    case 49:
      if (lookahead == 'v') ADVANCE(23);
      END_STATE();
    case 50:
      if (lookahead == 'y') ADVANCE(109);
      END_STATE();
    case 51:
      if (lookahead == 'y') ADVANCE(110);
      END_STATE();
    case 52:
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(53);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(101);
      END_STATE();
    case 53:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(101);
      END_STATE();
    case 54:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(94);
      END_STATE();
    case 55:
      if (lookahead != 0 &&
          lookahead != '\r') ADVANCE(103);
      if (lookahead == '\r') ADVANCE(104);
      END_STATE();
    case 56:
      if (eof) ADVANCE(61);
      if (lookahead == '\n') SKIP(0)
      END_STATE();
    case 57:
      if (eof) ADVANCE(61);
      if (lookahead == '\n') SKIP(0)
      if (lookahead == '\r') SKIP(56)
      END_STATE();
    case 58:
      if (eof) ADVANCE(61);
      if (lookahead == '\n') SKIP(60)
      END_STATE();
    case 59:
      if (eof) ADVANCE(61);
      if (lookahead == '\n') SKIP(60)
      if (lookahead == '\r') SKIP(58)
      END_STATE();
    case 60:
      if (eof) ADVANCE(61);
      if (lookahead == '!') ADVANCE(73);
      if (lookahead == '"') ADVANCE(82);
      if (lookahead == '&') ADVANCE(70);
      if (lookahead == '(') ADVANCE(80);
      if (lookahead == ')') ADVANCE(81);
      if (lookahead == '*') ADVANCE(67);
      if (lookahead == '+') ADVANCE(77);
      if (lookahead == ',') ADVANCE(63);
      if (lookahead == '-') ADVANCE(78);
      if (lookahead == '.') ADVANCE(105);
      if (lookahead == '/') ADVANCE(79);
      if (lookahead == '0') ADVANCE(90);
      if (lookahead == '<') ADVANCE(75);
      if (lookahead == '=') ADVANCE(76);
      if (lookahead == '>') ADVANCE(74);
      if (lookahead == 'S') ADVANCE(117);
      if (lookahead == '[') ADVANCE(65);
      if (lookahead == '\\') SKIP(59)
      if (lookahead == ']') ADVANCE(68);
      if (lookahead == 'a') ADVANCE(126);
      if (lookahead == 'f') ADVANCE(122);
      if (lookahead == 'g') ADVANCE(131);
      if (lookahead == '{') ADVANCE(62);
      if (lookahead == '|') ADVANCE(69);
      if (lookahead == '}') ADVANCE(64);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ' ||
          lookahead == 8203 ||
          lookahead == 8288 ||
          lookahead == 65279) SKIP(60)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(91);
      if (sym_name_character_set_3(lookahead)) ADVANCE(139);
      END_STATE();
    case 61:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 62:
      ACCEPT_TOKEN(anon_sym_LBRACE);
      END_STATE();
    case 63:
      ACCEPT_TOKEN(anon_sym_COMMA);
      END_STATE();
    case 64:
      ACCEPT_TOKEN(anon_sym_RBRACE);
      END_STATE();
    case 65:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      END_STATE();
    case 66:
      ACCEPT_TOKEN(anon_sym_DOT_DOT);
      END_STATE();
    case 67:
      ACCEPT_TOKEN(anon_sym_STAR);
      END_STATE();
    case 68:
      ACCEPT_TOKEN(anon_sym_RBRACK);
      END_STATE();
    case 69:
      ACCEPT_TOKEN(anon_sym_PIPE);
      END_STATE();
    case 70:
      ACCEPT_TOKEN(anon_sym_AMP);
      END_STATE();
    case 71:
      ACCEPT_TOKEN(anon_sym_EQ_GT);
      END_STATE();
    case 72:
      ACCEPT_TOKEN(anon_sym_LT_EQ_GT);
      END_STATE();
    case 73:
      ACCEPT_TOKEN(anon_sym_BANG);
      END_STATE();
    case 74:
      ACCEPT_TOKEN(anon_sym_GT);
      END_STATE();
    case 75:
      ACCEPT_TOKEN(anon_sym_LT);
      if (lookahead == '=') ADVANCE(11);
      END_STATE();
    case 76:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '>') ADVANCE(71);
      END_STATE();
    case 77:
      ACCEPT_TOKEN(anon_sym_PLUS);
      END_STATE();
    case 78:
      ACCEPT_TOKEN(anon_sym_DASH);
      END_STATE();
    case 79:
      ACCEPT_TOKEN(anon_sym_SLASH);
      if (lookahead == '*') ADVANCE(9);
      if (lookahead == '/') ADVANCE(103);
      END_STATE();
    case 80:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 81:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 82:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      END_STATE();
    case 83:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '*') ADVANCE(85);
      if (lookahead == '/') ADVANCE(87);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != '"' &&
          lookahead != '?') ADVANCE(89);
      END_STATE();
    case 84:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '*') ADVANCE(84);
      if (lookahead == '/') ADVANCE(89);
      if (lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == '"' ||
          lookahead == '?') ADVANCE(9);
      if (lookahead != 0) ADVANCE(85);
      END_STATE();
    case 85:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '*') ADVANCE(84);
      if (lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == '"' ||
          lookahead == '?') ADVANCE(9);
      if (lookahead != 0) ADVANCE(85);
      END_STATE();
    case 86:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '/') ADVANCE(83);
      if (lookahead == '\\') ADVANCE(89);
      if (lookahead == '\t' ||
          lookahead == '\f' ||
          lookahead == ' ' ||
          lookahead == 8203 ||
          lookahead == 8288 ||
          lookahead == 65279) ADVANCE(86);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != '"' &&
          lookahead != '?') ADVANCE(89);
      END_STATE();
    case 87:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '\\') ADVANCE(88);
      if (lookahead == '\r' ||
          lookahead == '"' ||
          lookahead == '?') ADVANCE(103);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(87);
      END_STATE();
    case 88:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '\n' ||
          lookahead == '"' ||
          lookahead == '?') ADVANCE(103);
      if (lookahead == '\r') ADVANCE(104);
      if (lookahead != 0) ADVANCE(87);
      END_STATE();
    case 89:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '\r' &&
          lookahead != '"' &&
          lookahead != '?') ADVANCE(89);
      END_STATE();
    case 90:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == '.') ADVANCE(99);
      if (lookahead == '_') ADVANCE(91);
      if (lookahead == 'X' ||
          lookahead == 'x') ADVANCE(54);
      if (lookahead == 'E' ||
          lookahead == 'P' ||
          lookahead == 'e' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(91);
      END_STATE();
    case 91:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == '.') ADVANCE(99);
      if (lookahead == '_') ADVANCE(91);
      if (lookahead == 'E' ||
          lookahead == 'P' ||
          lookahead == 'e' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(91);
      END_STATE();
    case 92:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == '.') ADVANCE(97);
      if (lookahead == '_') ADVANCE(94);
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(53);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(92);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(94);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(93);
      END_STATE();
    case 93:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == '.') ADVANCE(97);
      if (lookahead == '_') ADVANCE(94);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(92);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(94);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(93);
      END_STATE();
    case 94:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == '.') ADVANCE(97);
      if (lookahead == '_') ADVANCE(94);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(92);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(94);
      END_STATE();
    case 95:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(53);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(95);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(96);
      if (('A' <= lookahead && lookahead <= 'F') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(98);
      END_STATE();
    case 96:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(95);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(96);
      if (('A' <= lookahead && lookahead <= 'F') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(98);
      END_STATE();
    case 97:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(95);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(98);
      END_STATE();
    case 98:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(95);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(98);
      END_STATE();
    case 99:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == 'E' ||
          lookahead == 'P' ||
          lookahead == 'e' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(100);
      END_STATE();
    case 100:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == 'E' ||
          lookahead == 'P' ||
          lookahead == 'e' ||
          lookahead == 'p') ADVANCE(52);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(100);
      END_STATE();
    case 101:
      ACCEPT_TOKEN(sym_number);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(101);
      END_STATE();
    case 102:
      ACCEPT_TOKEN(sym_comment);
      END_STATE();
    case 103:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead == '\\') ADVANCE(55);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(103);
      END_STATE();
    case 104:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\\') ADVANCE(103);
      if (lookahead == '\\') ADVANCE(55);
      END_STATE();
    case 105:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 106:
      ACCEPT_TOKEN(anon_sym_DOT);
      if (lookahead == '.') ADVANCE(66);
      END_STATE();
    case 107:
      ACCEPT_TOKEN(anon_sym_SMT_DASHlevel);
      END_STATE();
    case 108:
      ACCEPT_TOKEN(anon_sym_SAT_DASHlevel);
      END_STATE();
    case 109:
      ACCEPT_TOKEN(anon_sym_group_DASHcardinality);
      END_STATE();
    case 110:
      ACCEPT_TOKEN(anon_sym_feature_DASHcardinality);
      END_STATE();
    case 111:
      ACCEPT_TOKEN(anon_sym_aggregate_DASHfunction);
      END_STATE();
    case 112:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == '-') ADVANCE(31);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 113:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == '-') ADVANCE(16);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 114:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == '-') ADVANCE(25);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 115:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == '-') ADVANCE(36);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 116:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == '-') ADVANCE(18);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 117:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'A') ADVANCE(118);
      if (lookahead == 'M') ADVANCE(119);
      if (sym_name_character_set_5(lookahead)) ADVANCE(139);
      END_STATE();
    case 118:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'T') ADVANCE(112);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 119:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'T') ADVANCE(115);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 120:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'a') ADVANCE(134);
      if (sym_name_character_set_6(lookahead)) ADVANCE(139);
      END_STATE();
    case 121:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'a') ADVANCE(135);
      if (sym_name_character_set_6(lookahead)) ADVANCE(139);
      END_STATE();
    case 122:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'e') ADVANCE(120);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 123:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'e') ADVANCE(128);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 124:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'e') ADVANCE(114);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 125:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'e') ADVANCE(116);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 126:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'g') ADVANCE(127);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 127:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'g') ADVANCE(132);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 128:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'g') ADVANCE(121);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 129:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'o') ADVANCE(136);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 130:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'p') ADVANCE(113);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 131:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'r') ADVANCE(129);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 132:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'r') ADVANCE(123);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 133:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'r') ADVANCE(125);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 134:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 't') ADVANCE(137);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 135:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 't') ADVANCE(124);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 136:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'u') ADVANCE(130);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 137:
      ACCEPT_TOKEN(sym_name);
      if (lookahead == 'u') ADVANCE(133);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 138:
      ACCEPT_TOKEN(sym_name);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(138);
      if (sym_name_character_set_7(lookahead)) ADVANCE(139);
      END_STATE();
    case 139:
      ACCEPT_TOKEN(sym_name);
      if (sym_name_character_set_4(lookahead)) ADVANCE(139);
      END_STATE();
    case 140:
      ACCEPT_TOKEN(sym_int);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(140);
      END_STATE();
    default:
      return false;
  }
}

static bool ts_lex_keywords(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (lookahead == '\\') SKIP(1)
      if (lookahead == 'a') ADVANCE(2);
      if (lookahead == 'c') ADVANCE(3);
      if (lookahead == 'f') ADVANCE(4);
      if (lookahead == 'i') ADVANCE(5);
      if (lookahead == 'm') ADVANCE(6);
      if (lookahead == 'n') ADVANCE(7);
      if (lookahead == 'o') ADVANCE(8);
      if (lookahead == 's') ADVANCE(9);
      if (lookahead == 't') ADVANCE(10);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\f' ||
          lookahead == '\r' ||
          lookahead == ' ' ||
          lookahead == 8203 ||
          lookahead == 8288 ||
          lookahead == 65279) SKIP(0)
      END_STATE();
    case 1:
      if (lookahead == '\n') SKIP(0)
      if (lookahead == '\r') SKIP(11)
      END_STATE();
    case 2:
      if (lookahead == 'l') ADVANCE(12);
      if (lookahead == 's') ADVANCE(13);
      if (lookahead == 'v') ADVANCE(14);
      END_STATE();
    case 3:
      if (lookahead == 'o') ADVANCE(15);
      END_STATE();
    case 4:
      if (lookahead == 'a') ADVANCE(16);
      if (lookahead == 'e') ADVANCE(17);
      END_STATE();
    case 5:
      if (lookahead == 'm') ADVANCE(18);
      if (lookahead == 'n') ADVANCE(19);
      END_STATE();
    case 6:
      if (lookahead == 'a') ADVANCE(20);
      END_STATE();
    case 7:
      if (lookahead == 'a') ADVANCE(21);
      END_STATE();
    case 8:
      if (lookahead == 'p') ADVANCE(22);
      if (lookahead == 'r') ADVANCE(23);
      END_STATE();
    case 9:
      if (lookahead == 'u') ADVANCE(24);
      END_STATE();
    case 10:
      if (lookahead == 'r') ADVANCE(25);
      END_STATE();
    case 11:
      if (lookahead == '\n') SKIP(0)
      END_STATE();
    case 12:
      if (lookahead == 't') ADVANCE(26);
      END_STATE();
    case 13:
      ACCEPT_TOKEN(anon_sym_as);
      END_STATE();
    case 14:
      if (lookahead == 'g') ADVANCE(27);
      END_STATE();
    case 15:
      if (lookahead == 'n') ADVANCE(28);
      END_STATE();
    case 16:
      if (lookahead == 'l') ADVANCE(29);
      END_STATE();
    case 17:
      if (lookahead == 'a') ADVANCE(30);
      END_STATE();
    case 18:
      if (lookahead == 'p') ADVANCE(31);
      END_STATE();
    case 19:
      if (lookahead == 'c') ADVANCE(32);
      END_STATE();
    case 20:
      if (lookahead == 'n') ADVANCE(33);
      END_STATE();
    case 21:
      if (lookahead == 'm') ADVANCE(34);
      END_STATE();
    case 22:
      if (lookahead == 't') ADVANCE(35);
      END_STATE();
    case 23:
      ACCEPT_TOKEN(anon_sym_or);
      END_STATE();
    case 24:
      if (lookahead == 'm') ADVANCE(36);
      END_STATE();
    case 25:
      if (lookahead == 'u') ADVANCE(37);
      END_STATE();
    case 26:
      if (lookahead == 'e') ADVANCE(38);
      END_STATE();
    case 27:
      ACCEPT_TOKEN(anon_sym_avg);
      END_STATE();
    case 28:
      if (lookahead == 's') ADVANCE(39);
      END_STATE();
    case 29:
      if (lookahead == 's') ADVANCE(40);
      END_STATE();
    case 30:
      if (lookahead == 't') ADVANCE(41);
      END_STATE();
    case 31:
      if (lookahead == 'o') ADVANCE(42);
      END_STATE();
    case 32:
      if (lookahead == 'l') ADVANCE(43);
      END_STATE();
    case 33:
      if (lookahead == 'd') ADVANCE(44);
      END_STATE();
    case 34:
      if (lookahead == 'e') ADVANCE(45);
      END_STATE();
    case 35:
      if (lookahead == 'i') ADVANCE(46);
      END_STATE();
    case 36:
      ACCEPT_TOKEN(anon_sym_sum);
      END_STATE();
    case 37:
      if (lookahead == 'e') ADVANCE(47);
      END_STATE();
    case 38:
      if (lookahead == 'r') ADVANCE(48);
      END_STATE();
    case 39:
      if (lookahead == 't') ADVANCE(49);
      END_STATE();
    case 40:
      if (lookahead == 'e') ADVANCE(50);
      END_STATE();
    case 41:
      if (lookahead == 'u') ADVANCE(51);
      END_STATE();
    case 42:
      if (lookahead == 'r') ADVANCE(52);
      END_STATE();
    case 43:
      if (lookahead == 'u') ADVANCE(53);
      END_STATE();
    case 44:
      if (lookahead == 'a') ADVANCE(54);
      END_STATE();
    case 45:
      if (lookahead == 's') ADVANCE(55);
      END_STATE();
    case 46:
      if (lookahead == 'o') ADVANCE(56);
      END_STATE();
    case 47:
      ACCEPT_TOKEN(anon_sym_true);
      END_STATE();
    case 48:
      if (lookahead == 'n') ADVANCE(57);
      END_STATE();
    case 49:
      if (lookahead == 'r') ADVANCE(58);
      END_STATE();
    case 50:
      ACCEPT_TOKEN(anon_sym_false);
      END_STATE();
    case 51:
      if (lookahead == 'r') ADVANCE(59);
      END_STATE();
    case 52:
      if (lookahead == 't') ADVANCE(60);
      END_STATE();
    case 53:
      if (lookahead == 'd') ADVANCE(61);
      END_STATE();
    case 54:
      if (lookahead == 't') ADVANCE(62);
      END_STATE();
    case 55:
      if (lookahead == 'p') ADVANCE(63);
      END_STATE();
    case 56:
      if (lookahead == 'n') ADVANCE(64);
      END_STATE();
    case 57:
      if (lookahead == 'a') ADVANCE(65);
      END_STATE();
    case 58:
      if (lookahead == 'a') ADVANCE(66);
      END_STATE();
    case 59:
      if (lookahead == 'e') ADVANCE(67);
      END_STATE();
    case 60:
      if (lookahead == 's') ADVANCE(68);
      END_STATE();
    case 61:
      if (lookahead == 'e') ADVANCE(69);
      END_STATE();
    case 62:
      if (lookahead == 'o') ADVANCE(70);
      END_STATE();
    case 63:
      if (lookahead == 'a') ADVANCE(71);
      END_STATE();
    case 64:
      if (lookahead == 'a') ADVANCE(72);
      END_STATE();
    case 65:
      if (lookahead == 't') ADVANCE(73);
      END_STATE();
    case 66:
      if (lookahead == 'i') ADVANCE(74);
      END_STATE();
    case 67:
      if (lookahead == 's') ADVANCE(75);
      END_STATE();
    case 68:
      ACCEPT_TOKEN(sym_imports);
      END_STATE();
    case 69:
      ACCEPT_TOKEN(sym_include);
      END_STATE();
    case 70:
      if (lookahead == 'r') ADVANCE(76);
      END_STATE();
    case 71:
      if (lookahead == 'c') ADVANCE(77);
      END_STATE();
    case 72:
      if (lookahead == 'l') ADVANCE(78);
      END_STATE();
    case 73:
      if (lookahead == 'i') ADVANCE(79);
      END_STATE();
    case 74:
      if (lookahead == 'n') ADVANCE(80);
      END_STATE();
    case 75:
      ACCEPT_TOKEN(sym_features);
      END_STATE();
    case 76:
      if (lookahead == 'y') ADVANCE(81);
      END_STATE();
    case 77:
      if (lookahead == 'e') ADVANCE(82);
      END_STATE();
    case 78:
      ACCEPT_TOKEN(anon_sym_optional);
      END_STATE();
    case 79:
      if (lookahead == 'v') ADVANCE(83);
      END_STATE();
    case 80:
      if (lookahead == 't') ADVANCE(84);
      END_STATE();
    case 81:
      ACCEPT_TOKEN(anon_sym_mandatory);
      END_STATE();
    case 82:
      ACCEPT_TOKEN(anon_sym_namespace);
      END_STATE();
    case 83:
      if (lookahead == 'e') ADVANCE(85);
      END_STATE();
    case 84:
      ACCEPT_TOKEN(anon_sym_constraint);
      if (lookahead == 's') ADVANCE(86);
      END_STATE();
    case 85:
      ACCEPT_TOKEN(anon_sym_alternative);
      END_STATE();
    case 86:
      ACCEPT_TOKEN(anon_sym_constraints);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0, .external_lex_state = 1},
  [1] = {.lex_state = 60, .external_lex_state = 2},
  [2] = {.lex_state = 60, .external_lex_state = 3},
  [3] = {.lex_state = 60, .external_lex_state = 2},
  [4] = {.lex_state = 60, .external_lex_state = 3},
  [5] = {.lex_state = 60, .external_lex_state = 3},
  [6] = {.lex_state = 60, .external_lex_state = 2},
  [7] = {.lex_state = 60, .external_lex_state = 3},
  [8] = {.lex_state = 60, .external_lex_state = 3},
  [9] = {.lex_state = 60, .external_lex_state = 3},
  [10] = {.lex_state = 60, .external_lex_state = 3},
  [11] = {.lex_state = 60, .external_lex_state = 3},
  [12] = {.lex_state = 60, .external_lex_state = 3},
  [13] = {.lex_state = 60, .external_lex_state = 4},
  [14] = {.lex_state = 60, .external_lex_state = 5},
  [15] = {.lex_state = 60, .external_lex_state = 5},
  [16] = {.lex_state = 60, .external_lex_state = 4},
  [17] = {.lex_state = 60, .external_lex_state = 5},
  [18] = {.lex_state = 60, .external_lex_state = 5},
  [19] = {.lex_state = 60, .external_lex_state = 4},
  [20] = {.lex_state = 60, .external_lex_state = 5},
  [21] = {.lex_state = 60, .external_lex_state = 4},
  [22] = {.lex_state = 60, .external_lex_state = 5},
  [23] = {.lex_state = 60, .external_lex_state = 4},
  [24] = {.lex_state = 60, .external_lex_state = 4},
  [25] = {.lex_state = 5, .external_lex_state = 6},
  [26] = {.lex_state = 60, .external_lex_state = 2},
  [27] = {.lex_state = 60, .external_lex_state = 3},
  [28] = {.lex_state = 60, .external_lex_state = 3},
  [29] = {.lex_state = 60, .external_lex_state = 2},
  [30] = {.lex_state = 60, .external_lex_state = 2},
  [31] = {.lex_state = 60, .external_lex_state = 3},
  [32] = {.lex_state = 60, .external_lex_state = 3},
  [33] = {.lex_state = 60, .external_lex_state = 2},
  [34] = {.lex_state = 5, .external_lex_state = 7},
  [35] = {.lex_state = 5, .external_lex_state = 6},
  [36] = {.lex_state = 5, .external_lex_state = 7},
  [37] = {.lex_state = 5, .external_lex_state = 6},
  [38] = {.lex_state = 5, .external_lex_state = 7},
  [39] = {.lex_state = 60, .external_lex_state = 8},
  [40] = {.lex_state = 5, .external_lex_state = 7},
  [41] = {.lex_state = 5, .external_lex_state = 2},
  [42] = {.lex_state = 5, .external_lex_state = 2},
  [43] = {.lex_state = 5, .external_lex_state = 2},
  [44] = {.lex_state = 5, .external_lex_state = 7},
  [45] = {.lex_state = 5, .external_lex_state = 7},
  [46] = {.lex_state = 5, .external_lex_state = 2},
  [47] = {.lex_state = 5, .external_lex_state = 2},
  [48] = {.lex_state = 5, .external_lex_state = 2},
  [49] = {.lex_state = 5, .external_lex_state = 2},
  [50] = {.lex_state = 5, .external_lex_state = 2},
  [51] = {.lex_state = 5, .external_lex_state = 2},
  [52] = {.lex_state = 5, .external_lex_state = 2},
  [53] = {.lex_state = 5, .external_lex_state = 2},
  [54] = {.lex_state = 5, .external_lex_state = 2},
  [55] = {.lex_state = 5, .external_lex_state = 2},
  [56] = {.lex_state = 5, .external_lex_state = 2},
  [57] = {.lex_state = 5, .external_lex_state = 2},
  [58] = {.lex_state = 5, .external_lex_state = 2},
  [59] = {.lex_state = 5, .external_lex_state = 2},
  [60] = {.lex_state = 5, .external_lex_state = 2},
  [61] = {.lex_state = 5, .external_lex_state = 2},
  [62] = {.lex_state = 5, .external_lex_state = 2},
  [63] = {.lex_state = 5, .external_lex_state = 2},
  [64] = {.lex_state = 5, .external_lex_state = 2},
  [65] = {.lex_state = 5, .external_lex_state = 2},
  [66] = {.lex_state = 5, .external_lex_state = 2},
  [67] = {.lex_state = 5, .external_lex_state = 2},
  [68] = {.lex_state = 5, .external_lex_state = 2},
  [69] = {.lex_state = 5, .external_lex_state = 2},
  [70] = {.lex_state = 5, .external_lex_state = 2},
  [71] = {.lex_state = 5, .external_lex_state = 2},
  [72] = {.lex_state = 5, .external_lex_state = 2},
  [73] = {.lex_state = 5, .external_lex_state = 2},
  [74] = {.lex_state = 5, .external_lex_state = 2},
  [75] = {.lex_state = 5, .external_lex_state = 2},
  [76] = {.lex_state = 5, .external_lex_state = 2},
  [77] = {.lex_state = 5, .external_lex_state = 8},
  [78] = {.lex_state = 5, .external_lex_state = 9},
  [79] = {.lex_state = 5, .external_lex_state = 8},
  [80] = {.lex_state = 5, .external_lex_state = 9},
  [81] = {.lex_state = 5, .external_lex_state = 8},
  [82] = {.lex_state = 5, .external_lex_state = 6},
  [83] = {.lex_state = 5, .external_lex_state = 7},
  [84] = {.lex_state = 5, .external_lex_state = 6},
  [85] = {.lex_state = 5, .external_lex_state = 7},
  [86] = {.lex_state = 5, .external_lex_state = 8},
  [87] = {.lex_state = 5, .external_lex_state = 8},
  [88] = {.lex_state = 5, .external_lex_state = 8},
  [89] = {.lex_state = 5, .external_lex_state = 8},
  [90] = {.lex_state = 60, .external_lex_state = 7},
  [91] = {.lex_state = 60, .external_lex_state = 7},
  [92] = {.lex_state = 5, .external_lex_state = 8},
  [93] = {.lex_state = 60, .external_lex_state = 6},
  [94] = {.lex_state = 60, .external_lex_state = 6},
  [95] = {.lex_state = 60, .external_lex_state = 7},
  [96] = {.lex_state = 60, .external_lex_state = 9},
  [97] = {.lex_state = 5, .external_lex_state = 8},
  [98] = {.lex_state = 60, .external_lex_state = 6},
  [99] = {.lex_state = 60, .external_lex_state = 9},
  [100] = {.lex_state = 60, .external_lex_state = 9},
  [101] = {.lex_state = 60, .external_lex_state = 8},
  [102] = {.lex_state = 60, .external_lex_state = 9},
  [103] = {.lex_state = 60, .external_lex_state = 6},
  [104] = {.lex_state = 60, .external_lex_state = 7},
  [105] = {.lex_state = 60, .external_lex_state = 8},
  [106] = {.lex_state = 60, .external_lex_state = 7},
  [107] = {.lex_state = 60, .external_lex_state = 6},
  [108] = {.lex_state = 60, .external_lex_state = 7},
  [109] = {.lex_state = 60, .external_lex_state = 8},
  [110] = {.lex_state = 5, .external_lex_state = 8},
  [111] = {.lex_state = 0, .external_lex_state = 7},
  [112] = {.lex_state = 60, .external_lex_state = 7},
  [113] = {.lex_state = 60, .external_lex_state = 9},
  [114] = {.lex_state = 60, .external_lex_state = 9},
  [115] = {.lex_state = 0, .external_lex_state = 8},
  [116] = {.lex_state = 0, .external_lex_state = 8},
  [117] = {.lex_state = 0, .external_lex_state = 7},
  [118] = {.lex_state = 0, .external_lex_state = 7},
  [119] = {.lex_state = 0, .external_lex_state = 7},
  [120] = {.lex_state = 0, .external_lex_state = 7},
  [121] = {.lex_state = 0, .external_lex_state = 9},
  [122] = {.lex_state = 0, .external_lex_state = 7},
  [123] = {.lex_state = 0, .external_lex_state = 8},
  [124] = {.lex_state = 0, .external_lex_state = 7},
  [125] = {.lex_state = 0, .external_lex_state = 7},
  [126] = {.lex_state = 0, .external_lex_state = 9},
  [127] = {.lex_state = 0, .external_lex_state = 7},
  [128] = {.lex_state = 0, .external_lex_state = 9},
  [129] = {.lex_state = 0, .external_lex_state = 9},
  [130] = {.lex_state = 0, .external_lex_state = 9},
  [131] = {.lex_state = 0, .external_lex_state = 9},
  [132] = {.lex_state = 0, .external_lex_state = 9},
  [133] = {.lex_state = 0, .external_lex_state = 9},
  [134] = {.lex_state = 0, .external_lex_state = 6},
  [135] = {.lex_state = 0, .external_lex_state = 7},
  [136] = {.lex_state = 0, .external_lex_state = 8},
  [137] = {.lex_state = 0, .external_lex_state = 7},
  [138] = {.lex_state = 0, .external_lex_state = 6},
  [139] = {.lex_state = 0, .external_lex_state = 9},
  [140] = {.lex_state = 0, .external_lex_state = 9},
  [141] = {.lex_state = 0, .external_lex_state = 9},
  [142] = {.lex_state = 0, .external_lex_state = 9},
  [143] = {.lex_state = 0, .external_lex_state = 8},
  [144] = {.lex_state = 0, .external_lex_state = 6},
  [145] = {.lex_state = 0, .external_lex_state = 9},
  [146] = {.lex_state = 0, .external_lex_state = 8},
  [147] = {.lex_state = 0, .external_lex_state = 9},
  [148] = {.lex_state = 0, .external_lex_state = 7},
  [149] = {.lex_state = 0, .external_lex_state = 6},
  [150] = {.lex_state = 0, .external_lex_state = 8},
  [151] = {.lex_state = 0, .external_lex_state = 6},
  [152] = {.lex_state = 0, .external_lex_state = 8},
  [153] = {.lex_state = 0, .external_lex_state = 8},
  [154] = {.lex_state = 0, .external_lex_state = 7},
  [155] = {.lex_state = 0, .external_lex_state = 9},
  [156] = {.lex_state = 0, .external_lex_state = 8},
  [157] = {.lex_state = 0, .external_lex_state = 6},
  [158] = {.lex_state = 0, .external_lex_state = 6},
  [159] = {.lex_state = 0, .external_lex_state = 9},
  [160] = {.lex_state = 0, .external_lex_state = 6},
  [161] = {.lex_state = 0, .external_lex_state = 6},
  [162] = {.lex_state = 0, .external_lex_state = 6},
  [163] = {.lex_state = 0, .external_lex_state = 6},
  [164] = {.lex_state = 0, .external_lex_state = 6},
  [165] = {.lex_state = 0, .external_lex_state = 7},
  [166] = {.lex_state = 0, .external_lex_state = 6},
  [167] = {.lex_state = 0, .external_lex_state = 7},
  [168] = {.lex_state = 0, .external_lex_state = 8},
  [169] = {.lex_state = 0, .external_lex_state = 8},
  [170] = {.lex_state = 0, .external_lex_state = 6},
  [171] = {.lex_state = 0, .external_lex_state = 8},
  [172] = {.lex_state = 60, .external_lex_state = 2},
  [173] = {.lex_state = 0, .external_lex_state = 9},
  [174] = {.lex_state = 0, .external_lex_state = 9},
  [175] = {.lex_state = 0, .external_lex_state = 9},
  [176] = {.lex_state = 0, .external_lex_state = 9},
  [177] = {.lex_state = 0, .external_lex_state = 9},
  [178] = {.lex_state = 0, .external_lex_state = 9},
  [179] = {.lex_state = 0, .external_lex_state = 9},
  [180] = {.lex_state = 0, .external_lex_state = 9},
  [181] = {.lex_state = 5, .external_lex_state = 6},
  [182] = {.lex_state = 5, .external_lex_state = 6},
  [183] = {.lex_state = 5, .external_lex_state = 6},
  [184] = {.lex_state = 5, .external_lex_state = 6},
  [185] = {.lex_state = 5, .external_lex_state = 6},
  [186] = {.lex_state = 5, .external_lex_state = 6},
  [187] = {.lex_state = 5, .external_lex_state = 6},
  [188] = {.lex_state = 5, .external_lex_state = 6},
  [189] = {.lex_state = 5, .external_lex_state = 2},
  [190] = {.lex_state = 5, .external_lex_state = 2},
  [191] = {.lex_state = 5, .external_lex_state = 2},
  [192] = {.lex_state = 5, .external_lex_state = 2},
  [193] = {.lex_state = 5, .external_lex_state = 2},
  [194] = {.lex_state = 5, .external_lex_state = 8},
  [195] = {.lex_state = 5, .external_lex_state = 8},
  [196] = {.lex_state = 5, .external_lex_state = 2},
  [197] = {.lex_state = 5, .external_lex_state = 2},
  [198] = {.lex_state = 5, .external_lex_state = 2},
  [199] = {.lex_state = 60, .external_lex_state = 8},
  [200] = {.lex_state = 60, .external_lex_state = 8},
  [201] = {.lex_state = 5, .external_lex_state = 2},
  [202] = {.lex_state = 60, .external_lex_state = 8},
  [203] = {.lex_state = 0, .external_lex_state = 6},
  [204] = {.lex_state = 0, .external_lex_state = 6},
  [205] = {.lex_state = 0, .external_lex_state = 7},
  [206] = {.lex_state = 60, .external_lex_state = 8},
  [207] = {.lex_state = 0, .external_lex_state = 6},
  [208] = {.lex_state = 0, .external_lex_state = 7},
  [209] = {.lex_state = 0, .external_lex_state = 7},
  [210] = {.lex_state = 0, .external_lex_state = 6},
  [211] = {.lex_state = 60, .external_lex_state = 8},
  [212] = {.lex_state = 0, .external_lex_state = 6},
  [213] = {.lex_state = 0, .external_lex_state = 7},
  [214] = {.lex_state = 0, .external_lex_state = 6},
  [215] = {.lex_state = 60, .external_lex_state = 8},
  [216] = {.lex_state = 0, .external_lex_state = 8},
  [217] = {.lex_state = 0, .external_lex_state = 7},
  [218] = {.lex_state = 0, .external_lex_state = 8},
  [219] = {.lex_state = 0, .external_lex_state = 6},
  [220] = {.lex_state = 0, .external_lex_state = 6},
  [221] = {.lex_state = 0, .external_lex_state = 7},
  [222] = {.lex_state = 0, .external_lex_state = 7},
  [223] = {.lex_state = 0, .external_lex_state = 6},
  [224] = {.lex_state = 0, .external_lex_state = 8},
  [225] = {.lex_state = 0, .external_lex_state = 6},
  [226] = {.lex_state = 0, .external_lex_state = 8},
  [227] = {.lex_state = 0, .external_lex_state = 7},
  [228] = {.lex_state = 0, .external_lex_state = 7},
  [229] = {.lex_state = 0, .external_lex_state = 7},
  [230] = {.lex_state = 0, .external_lex_state = 6},
  [231] = {.lex_state = 0, .external_lex_state = 6},
  [232] = {.lex_state = 0, .external_lex_state = 6},
  [233] = {.lex_state = 0, .external_lex_state = 6},
  [234] = {.lex_state = 0, .external_lex_state = 6},
  [235] = {.lex_state = 0, .external_lex_state = 6},
  [236] = {.lex_state = 0, .external_lex_state = 8},
  [237] = {.lex_state = 6, .external_lex_state = 2},
  [238] = {.lex_state = 0, .external_lex_state = 6},
  [239] = {.lex_state = 6, .external_lex_state = 2},
  [240] = {.lex_state = 0, .external_lex_state = 6},
  [241] = {.lex_state = 6, .external_lex_state = 7},
  [242] = {.lex_state = 0, .external_lex_state = 8},
  [243] = {.lex_state = 0, .external_lex_state = 6},
  [244] = {.lex_state = 0, .external_lex_state = 6},
  [245] = {.lex_state = 0, .external_lex_state = 7},
  [246] = {.lex_state = 0, .external_lex_state = 6},
  [247] = {.lex_state = 0, .external_lex_state = 7},
  [248] = {.lex_state = 0, .external_lex_state = 7},
  [249] = {.lex_state = 0, .external_lex_state = 7},
  [250] = {.lex_state = 0, .external_lex_state = 7},
  [251] = {.lex_state = 0, .external_lex_state = 6},
  [252] = {.lex_state = 0, .external_lex_state = 6},
  [253] = {.lex_state = 0, .external_lex_state = 8},
  [254] = {.lex_state = 0, .external_lex_state = 7},
  [255] = {.lex_state = 0, .external_lex_state = 8},
  [256] = {.lex_state = 86, .external_lex_state = 2},
  [257] = {.lex_state = 0, .external_lex_state = 2},
  [258] = {.lex_state = 0, .external_lex_state = 2},
  [259] = {.lex_state = 86, .external_lex_state = 2},
  [260] = {.lex_state = 86, .external_lex_state = 2},
  [261] = {.lex_state = 0, .external_lex_state = 2},
  [262] = {.lex_state = 0, .external_lex_state = 7},
  [263] = {.lex_state = 0, .external_lex_state = 2},
  [264] = {.lex_state = 0, .external_lex_state = 7},
  [265] = {.lex_state = 86, .external_lex_state = 2},
  [266] = {.lex_state = 0, .external_lex_state = 2},
  [267] = {.lex_state = 0, .external_lex_state = 2},
  [268] = {.lex_state = 0, .external_lex_state = 2},
  [269] = {.lex_state = 0, .external_lex_state = 2},
  [270] = {.lex_state = 0, .external_lex_state = 2},
  [271] = {.lex_state = 0, .external_lex_state = 2},
};

enum {
  ts_external_token__indent = 0,
  ts_external_token__dedent = 1,
  ts_external_token__newline = 2,
  ts_external_token_comment = 3,
  ts_external_token_RPAREN = 4,
  ts_external_token_RBRACK = 5,
  ts_external_token_RBRACE = 6,
};

static const TSSymbol ts_external_scanner_symbol_map[EXTERNAL_TOKEN_COUNT] = {
  [ts_external_token__indent] = sym__indent,
  [ts_external_token__dedent] = sym__dedent,
  [ts_external_token__newline] = sym__newline,
  [ts_external_token_comment] = sym_comment,
  [ts_external_token_RPAREN] = anon_sym_RPAREN,
  [ts_external_token_RBRACK] = anon_sym_RBRACK,
  [ts_external_token_RBRACE] = anon_sym_RBRACE,
};

static const bool ts_external_scanner_states[10][EXTERNAL_TOKEN_COUNT] = {
  [1] = {
    [ts_external_token__indent] = true,
    [ts_external_token__dedent] = true,
    [ts_external_token__newline] = true,
    [ts_external_token_comment] = true,
    [ts_external_token_RPAREN] = true,
    [ts_external_token_RBRACK] = true,
    [ts_external_token_RBRACE] = true,
  },
  [2] = {
    [ts_external_token_comment] = true,
  },
  [3] = {
    [ts_external_token__dedent] = true,
    [ts_external_token_comment] = true,
  },
  [4] = {
    [ts_external_token__indent] = true,
    [ts_external_token_comment] = true,
  },
  [5] = {
    [ts_external_token__indent] = true,
    [ts_external_token__dedent] = true,
    [ts_external_token_comment] = true,
  },
  [6] = {
    [ts_external_token_comment] = true,
    [ts_external_token_RBRACE] = true,
  },
  [7] = {
    [ts_external_token_comment] = true,
    [ts_external_token_RBRACK] = true,
  },
  [8] = {
    [ts_external_token__newline] = true,
    [ts_external_token_comment] = true,
  },
  [9] = {
    [ts_external_token_comment] = true,
    [ts_external_token_RPAREN] = true,
  },
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [sym_name] = ACTIONS(1),
    [anon_sym_LBRACE] = ACTIONS(1),
    [anon_sym_COMMA] = ACTIONS(1),
    [anon_sym_RBRACE] = ACTIONS(1),
    [anon_sym_as] = ACTIONS(1),
    [anon_sym_namespace] = ACTIONS(1),
    [anon_sym_LBRACK] = ACTIONS(1),
    [anon_sym_DOT_DOT] = ACTIONS(1),
    [anon_sym_STAR] = ACTIONS(1),
    [anon_sym_RBRACK] = ACTIONS(1),
    [anon_sym_constraint] = ACTIONS(1),
    [anon_sym_constraints] = ACTIONS(1),
    [anon_sym_PIPE] = ACTIONS(1),
    [anon_sym_AMP] = ACTIONS(1),
    [anon_sym_EQ_GT] = ACTIONS(1),
    [anon_sym_LT_EQ_GT] = ACTIONS(1),
    [anon_sym_BANG] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [anon_sym_EQ] = ACTIONS(1),
    [anon_sym_PLUS] = ACTIONS(1),
    [anon_sym_DASH] = ACTIONS(1),
    [anon_sym_SLASH] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [anon_sym_avg] = ACTIONS(1),
    [anon_sym_sum] = ACTIONS(1),
    [anon_sym_true] = ACTIONS(1),
    [anon_sym_false] = ACTIONS(1),
    [anon_sym_DQUOTE] = ACTIONS(1),
    [sym_number] = ACTIONS(1),
    [sym_comment] = ACTIONS(3),
    [anon_sym_DOT] = ACTIONS(1),
    [anon_sym_or] = ACTIONS(1),
    [anon_sym_alternative] = ACTIONS(1),
    [anon_sym_mandatory] = ACTIONS(1),
    [anon_sym_optional] = ACTIONS(1),
    [anon_sym_SMT_DASHlevel] = ACTIONS(1),
    [anon_sym_SAT_DASHlevel] = ACTIONS(1),
    [anon_sym_group_DASHcardinality] = ACTIONS(1),
    [anon_sym_feature_DASHcardinality] = ACTIONS(1),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(1),
    [sym_imports] = ACTIONS(1),
    [sym_features] = ACTIONS(1),
    [sym_include] = ACTIONS(1),
    [sym_int] = ACTIONS(1),
    [sym__indent] = ACTIONS(1),
    [sym__dedent] = ACTIONS(1),
    [sym__newline] = ACTIONS(1),
  },
  [1] = {
    [sym_source_file] = STATE(257),
    [sym_blk] = STATE(6),
    [sym__header] = STATE(216),
    [sym_ref] = STATE(216),
    [sym_namespace] = STATE(216),
    [sym_incomplete_namespace] = STATE(216),
    [sym_incomplete_ref] = STATE(216),
    [sym_cardinality] = STATE(216),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(216),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(216),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(216),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(6),
    [ts_builtin_sym_end] = ACTIONS(5),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(33),
    [sym_features] = ACTIONS(33),
    [sym_include] = ACTIONS(33),
  },
  [2] = {
    [sym_blk] = STATE(5),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(5),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(37),
  },
  [3] = {
    [sym_blk] = STATE(3),
    [sym__header] = STATE(216),
    [sym_ref] = STATE(216),
    [sym_namespace] = STATE(216),
    [sym_incomplete_namespace] = STATE(216),
    [sym_incomplete_ref] = STATE(216),
    [sym_cardinality] = STATE(216),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(216),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(216),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(216),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(3),
    [ts_builtin_sym_end] = ACTIONS(39),
    [sym_name] = ACTIONS(41),
    [anon_sym_namespace] = ACTIONS(44),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_STAR] = ACTIONS(50),
    [anon_sym_constraints] = ACTIONS(53),
    [anon_sym_BANG] = ACTIONS(56),
    [anon_sym_LPAREN] = ACTIONS(59),
    [anon_sym_avg] = ACTIONS(62),
    [anon_sym_sum] = ACTIONS(62),
    [anon_sym_true] = ACTIONS(65),
    [anon_sym_false] = ACTIONS(65),
    [anon_sym_DQUOTE] = ACTIONS(68),
    [sym_number] = ACTIONS(71),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(74),
    [anon_sym_alternative] = ACTIONS(74),
    [anon_sym_mandatory] = ACTIONS(74),
    [anon_sym_optional] = ACTIONS(74),
    [anon_sym_SMT_DASHlevel] = ACTIONS(77),
    [anon_sym_SAT_DASHlevel] = ACTIONS(77),
    [anon_sym_group_DASHcardinality] = ACTIONS(50),
    [anon_sym_feature_DASHcardinality] = ACTIONS(50),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(50),
    [sym_imports] = ACTIONS(80),
    [sym_features] = ACTIONS(80),
    [sym_include] = ACTIONS(80),
  },
  [4] = {
    [sym_blk] = STATE(8),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(8),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(83),
  },
  [5] = {
    [sym_blk] = STATE(7),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(7),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(85),
  },
  [6] = {
    [sym_blk] = STATE(3),
    [sym__header] = STATE(216),
    [sym_ref] = STATE(216),
    [sym_namespace] = STATE(216),
    [sym_incomplete_namespace] = STATE(216),
    [sym_incomplete_ref] = STATE(216),
    [sym_cardinality] = STATE(216),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(216),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(216),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(216),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(3),
    [ts_builtin_sym_end] = ACTIONS(87),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(33),
    [sym_features] = ACTIONS(33),
    [sym_include] = ACTIONS(33),
  },
  [7] = {
    [sym_blk] = STATE(7),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(7),
    [sym_name] = ACTIONS(41),
    [anon_sym_namespace] = ACTIONS(44),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_STAR] = ACTIONS(50),
    [anon_sym_constraints] = ACTIONS(53),
    [anon_sym_BANG] = ACTIONS(56),
    [anon_sym_LPAREN] = ACTIONS(59),
    [anon_sym_avg] = ACTIONS(62),
    [anon_sym_sum] = ACTIONS(62),
    [anon_sym_true] = ACTIONS(65),
    [anon_sym_false] = ACTIONS(65),
    [anon_sym_DQUOTE] = ACTIONS(68),
    [sym_number] = ACTIONS(71),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(74),
    [anon_sym_alternative] = ACTIONS(74),
    [anon_sym_mandatory] = ACTIONS(74),
    [anon_sym_optional] = ACTIONS(74),
    [anon_sym_SMT_DASHlevel] = ACTIONS(77),
    [anon_sym_SAT_DASHlevel] = ACTIONS(77),
    [anon_sym_group_DASHcardinality] = ACTIONS(50),
    [anon_sym_feature_DASHcardinality] = ACTIONS(50),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(50),
    [sym_imports] = ACTIONS(89),
    [sym_features] = ACTIONS(89),
    [sym_include] = ACTIONS(89),
    [sym__dedent] = ACTIONS(39),
  },
  [8] = {
    [sym_blk] = STATE(7),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(7),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(92),
  },
  [9] = {
    [sym_blk] = STATE(11),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(11),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(94),
  },
  [10] = {
    [sym_blk] = STATE(12),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(12),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(96),
  },
  [11] = {
    [sym_blk] = STATE(7),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(7),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(98),
  },
  [12] = {
    [sym_blk] = STATE(7),
    [sym__header] = STATE(218),
    [sym_ref] = STATE(218),
    [sym_namespace] = STATE(218),
    [sym_incomplete_namespace] = STATE(218),
    [sym_incomplete_ref] = STATE(218),
    [sym_cardinality] = STATE(218),
    [sym__expr] = STATE(136),
    [sym_constraint] = STATE(116),
    [sym_equation] = STATE(116),
    [sym_numeric] = STATE(116),
    [sym_nested_expr] = STATE(116),
    [sym__aggregate] = STATE(146),
    [sym_bool] = STATE(153),
    [sym_string] = STATE(88),
    [sym_lang_lvl] = STATE(218),
    [sym_path] = STATE(110),
    [sym_group_mode] = STATE(218),
    [sym_major_lvl] = STATE(200),
    [sym_minor_lvl] = STATE(200),
    [sym_constraints] = STATE(218),
    [sym__any_name] = STATE(81),
    [aux_sym_source_file_repeat1] = STATE(7),
    [sym_name] = ACTIONS(7),
    [anon_sym_namespace] = ACTIONS(9),
    [anon_sym_LBRACK] = ACTIONS(11),
    [anon_sym_STAR] = ACTIONS(13),
    [anon_sym_constraints] = ACTIONS(15),
    [anon_sym_BANG] = ACTIONS(17),
    [anon_sym_LPAREN] = ACTIONS(19),
    [anon_sym_avg] = ACTIONS(21),
    [anon_sym_sum] = ACTIONS(21),
    [anon_sym_true] = ACTIONS(23),
    [anon_sym_false] = ACTIONS(23),
    [anon_sym_DQUOTE] = ACTIONS(25),
    [sym_number] = ACTIONS(27),
    [sym_comment] = ACTIONS(3),
    [anon_sym_or] = ACTIONS(29),
    [anon_sym_alternative] = ACTIONS(29),
    [anon_sym_mandatory] = ACTIONS(29),
    [anon_sym_optional] = ACTIONS(29),
    [anon_sym_SMT_DASHlevel] = ACTIONS(31),
    [anon_sym_SAT_DASHlevel] = ACTIONS(31),
    [anon_sym_group_DASHcardinality] = ACTIONS(13),
    [anon_sym_feature_DASHcardinality] = ACTIONS(13),
    [anon_sym_aggregate_DASHfunction] = ACTIONS(13),
    [sym_imports] = ACTIONS(35),
    [sym_features] = ACTIONS(35),
    [sym_include] = ACTIONS(35),
    [sym__dedent] = ACTIONS(100),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(106), 1,
      sym__indent,
    ACTIONS(102), 12,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(104), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [37] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(112), 1,
      sym__indent,
    ACTIONS(110), 12,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(108), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [74] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(114), 1,
      sym__indent,
    ACTIONS(102), 12,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(104), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [111] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(116), 13,
      sym__indent,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(118), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [146] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(122), 13,
      sym__indent,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(120), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [181] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(126), 13,
      sym__indent,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(124), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [216] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(128), 13,
      sym__indent,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(130), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [251] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(128), 13,
      sym__indent,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(130), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [286] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(132), 1,
      sym__indent,
    ACTIONS(110), 12,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(108), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [323] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(116), 13,
      sym__indent,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(118), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [358] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(122), 13,
      sym__indent,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(120), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [393] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(126), 13,
      sym__indent,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(124), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [428] = 18,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 1,
      sym_name,
    ACTIONS(136), 1,
      anon_sym_LBRACE,
    ACTIONS(140), 1,
      anon_sym_LBRACK,
    ACTIONS(142), 1,
      anon_sym_BANG,
    ACTIONS(144), 1,
      anon_sym_LPAREN,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(152), 1,
      sym_number,
    STATE(93), 1,
      sym__any_name,
    STATE(103), 1,
      sym_string,
    STATE(134), 1,
      sym__expr,
    STATE(163), 1,
      sym__aggregate,
    STATE(164), 1,
      sym_bool,
    ACTIONS(138), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
    ACTIONS(146), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(148), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(235), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(162), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [492] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(154), 12,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(156), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [526] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(160), 12,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(158), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [560] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(154), 12,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(156), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [594] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(162), 12,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(164), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [628] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(160), 12,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(158), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [662] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(168), 12,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(166), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [696] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(162), 12,
      sym__dedent,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(164), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [730] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(168), 12,
      ts_builtin_sym_end,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_BANG,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(166), 14,
      anon_sym_namespace,
      anon_sym_constraints,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      anon_sym_or,
      anon_sym_alternative,
      anon_sym_mandatory,
      anon_sym_optional,
      sym_imports,
      sym_features,
      sym_include,
      sym_name,
  [764] = 18,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(176), 1,
      anon_sym_RBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(245), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [827] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(190), 8,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      sym_name,
    ACTIONS(192), 17,
      anon_sym_LBRACE,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_BANG,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_DOT,
  [860] = 18,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    ACTIONS(194), 1,
      anon_sym_RBRACK,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(245), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [923] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(196), 8,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
      anon_sym_avg,
      anon_sym_sum,
      anon_sym_true,
      anon_sym_false,
      sym_name,
    ACTIONS(198), 17,
      anon_sym_LBRACE,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_BANG,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_LPAREN,
      anon_sym_DQUOTE,
      sym_number,
      anon_sym_DOT,
  [956] = 18,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    ACTIONS(200), 1,
      anon_sym_RBRACK,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(245), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1019] = 11,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(202), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(92), 1,
      sym__any_name,
    ACTIONS(31), 2,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
    ACTIONS(204), 2,
      sym__newline,
      anon_sym_LBRACE,
    STATE(215), 2,
      sym_major_lvl,
      sym_minor_lvl,
    ACTIONS(13), 4,
      anon_sym_STAR,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
    ACTIONS(207), 4,
      anon_sym_as,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(209), 7,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [1068] = 18,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    ACTIONS(211), 1,
      anon_sym_RBRACK,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(245), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1131] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(205), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1191] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(222), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1251] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(172), 1,
      anon_sym_LBRACE,
    ACTIONS(174), 1,
      anon_sym_LBRACK,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(112), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(148), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(245), 3,
      sym_attributes,
      sym__value,
      sym_vector,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1311] = 15,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    ACTIONS(213), 1,
      anon_sym_RBRACK,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(119), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1363] = 15,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    ACTIONS(215), 1,
      anon_sym_RBRACK,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(119), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1415] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(133), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1464] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(111), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1513] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(121), 1,
      sym__expr,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1562] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(175), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1611] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(178), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1660] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_BANG,
    ACTIONS(19), 1,
      anon_sym_LPAREN,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(27), 1,
      sym_number,
    ACTIONS(231), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(101), 1,
      sym__any_name,
    STATE(146), 1,
      sym__aggregate,
    STATE(152), 1,
      sym__expr,
    STATE(153), 1,
      sym_bool,
    ACTIONS(21), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(23), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(116), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1709] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 1,
      sym_name,
    ACTIONS(142), 1,
      anon_sym_BANG,
    ACTIONS(144), 1,
      anon_sym_LPAREN,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(152), 1,
      sym_number,
    STATE(35), 1,
      sym_string,
    STATE(93), 1,
      sym__any_name,
    STATE(151), 1,
      sym__expr,
    STATE(163), 1,
      sym__aggregate,
    STATE(164), 1,
      sym_bool,
    ACTIONS(146), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(148), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(162), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1758] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(177), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1807] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(174), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1856] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(118), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1905] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_BANG,
    ACTIONS(19), 1,
      anon_sym_LPAREN,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(27), 1,
      sym_number,
    ACTIONS(231), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(101), 1,
      sym__any_name,
    STATE(146), 1,
      sym__aggregate,
    STATE(153), 1,
      sym_bool,
    STATE(168), 1,
      sym__expr,
    ACTIONS(21), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(23), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(116), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [1954] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(126), 1,
      sym__expr,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2003] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(179), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2052] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 1,
      sym_name,
    ACTIONS(142), 1,
      anon_sym_BANG,
    ACTIONS(144), 1,
      anon_sym_LPAREN,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(152), 1,
      sym_number,
    STATE(35), 1,
      sym_string,
    STATE(93), 1,
      sym__any_name,
    STATE(157), 1,
      sym__expr,
    STATE(163), 1,
      sym__aggregate,
    STATE(164), 1,
      sym_bool,
    ACTIONS(146), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(148), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(162), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2101] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 1,
      sym_name,
    ACTIONS(142), 1,
      anon_sym_BANG,
    ACTIONS(144), 1,
      anon_sym_LPAREN,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(152), 1,
      sym_number,
    STATE(35), 1,
      sym_string,
    STATE(93), 1,
      sym__any_name,
    STATE(158), 1,
      sym__expr,
    STATE(163), 1,
      sym__aggregate,
    STATE(164), 1,
      sym_bool,
    ACTIONS(146), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(148), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(162), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2150] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 1,
      sym_name,
    ACTIONS(142), 1,
      anon_sym_BANG,
    ACTIONS(144), 1,
      anon_sym_LPAREN,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(152), 1,
      sym_number,
    STATE(35), 1,
      sym_string,
    STATE(93), 1,
      sym__any_name,
    STATE(144), 1,
      sym__expr,
    STATE(163), 1,
      sym__aggregate,
    STATE(164), 1,
      sym_bool,
    ACTIONS(146), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(148), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(162), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2199] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(173), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2248] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_BANG,
    ACTIONS(19), 1,
      anon_sym_LPAREN,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(27), 1,
      sym_number,
    ACTIONS(231), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(101), 1,
      sym__any_name,
    STATE(146), 1,
      sym__aggregate,
    STATE(153), 1,
      sym_bool,
    STATE(169), 1,
      sym__expr,
    ACTIONS(21), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(23), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(116), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2297] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(119), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2346] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(159), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2395] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(176), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2444] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(140), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2493] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_BANG,
    ACTIONS(19), 1,
      anon_sym_LPAREN,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(27), 1,
      sym_number,
    ACTIONS(231), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(101), 1,
      sym__any_name,
    STATE(146), 1,
      sym__aggregate,
    STATE(153), 1,
      sym_bool,
    STATE(171), 1,
      sym__expr,
    ACTIONS(21), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(23), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(116), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2542] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(180), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2591] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(117), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2640] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    STATE(154), 1,
      sym__expr,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2689] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(147), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2738] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(170), 1,
      sym_name,
    ACTIONS(178), 1,
      anon_sym_BANG,
    ACTIONS(180), 1,
      anon_sym_LPAREN,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(188), 1,
      sym_number,
    STATE(95), 1,
      sym__any_name,
    STATE(106), 1,
      sym_string,
    STATE(122), 1,
      sym__expr,
    STATE(124), 1,
      sym_bool,
    STATE(125), 1,
      sym__aggregate,
    ACTIONS(182), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(184), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(127), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2787] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(141), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2836] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(217), 1,
      sym_name,
    ACTIONS(219), 1,
      anon_sym_BANG,
    ACTIONS(221), 1,
      anon_sym_LPAREN,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(229), 1,
      sym_number,
    STATE(100), 1,
      sym__any_name,
    STATE(113), 1,
      sym_string,
    STATE(131), 1,
      sym__aggregate,
    STATE(132), 1,
      sym_bool,
    STATE(142), 1,
      sym__expr,
    ACTIONS(223), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(225), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(130), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2885] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 1,
      sym_name,
    ACTIONS(142), 1,
      anon_sym_BANG,
    ACTIONS(144), 1,
      anon_sym_LPAREN,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(152), 1,
      sym_number,
    STATE(35), 1,
      sym_string,
    STATE(93), 1,
      sym__any_name,
    STATE(163), 1,
      sym__aggregate,
    STATE(164), 1,
      sym_bool,
    STATE(166), 1,
      sym__expr,
    ACTIONS(146), 2,
      anon_sym_avg,
      anon_sym_sum,
    ACTIONS(148), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(162), 5,
      sym_constraint,
      sym_equation,
      sym_numeric,
      sym_nested_expr,
      sym_path,
  [2934] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(233), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(89), 1,
      sym__any_name,
    ACTIONS(237), 4,
      anon_sym_as,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(235), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [2968] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(239), 1,
      sym_name,
    STATE(113), 1,
      sym_string,
    STATE(114), 1,
      sym__any_name,
    ACTIONS(237), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(235), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [3001] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(241), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(89), 1,
      sym__any_name,
    ACTIONS(207), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(209), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3034] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(239), 1,
      sym_name,
    STATE(113), 1,
      sym_string,
    STATE(114), 1,
      sym__any_name,
    ACTIONS(207), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(209), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [3067] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(249), 1,
      anon_sym_DOT,
    STATE(87), 1,
      aux_sym_path_repeat1,
    STATE(199), 1,
      aux_sym_lang_lvl_repeat1,
    ACTIONS(243), 2,
      sym__newline,
      anon_sym_LBRACE,
    ACTIONS(247), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(245), 9,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3100] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(251), 1,
      sym_name,
    STATE(35), 1,
      sym_string,
    STATE(107), 1,
      sym__any_name,
    ACTIONS(237), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(235), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3133] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(253), 1,
      sym_name,
    STATE(106), 1,
      sym_string,
    STATE(108), 1,
      sym__any_name,
    ACTIONS(237), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(235), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3166] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(251), 1,
      sym_name,
    STATE(35), 1,
      sym_string,
    STATE(107), 1,
      sym__any_name,
    ACTIONS(207), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(209), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3199] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(253), 1,
      sym_name,
    STATE(106), 1,
      sym_string,
    STATE(108), 1,
      sym__any_name,
    ACTIONS(207), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(209), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3232] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(259), 1,
      anon_sym_DOT,
    STATE(86), 1,
      aux_sym_path_repeat1,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 11,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3260] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(266), 1,
      anon_sym_DOT,
    STATE(86), 1,
      aux_sym_path_repeat1,
    ACTIONS(264), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(262), 11,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3288] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(190), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(192), 12,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3311] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 12,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3334] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(268), 1,
      anon_sym_DOT,
    STATE(90), 1,
      aux_sym_path_repeat1,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3361] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(271), 1,
      anon_sym_DOT,
    STATE(90), 1,
      aux_sym_path_repeat1,
    ACTIONS(264), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(262), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3388] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(273), 3,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_DOT,
    ACTIONS(255), 9,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3413] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(276), 1,
      anon_sym_DOT,
    STATE(94), 1,
      aux_sym_path_repeat1,
    ACTIONS(247), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(245), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3440] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(278), 1,
      anon_sym_DOT,
    STATE(98), 1,
      aux_sym_path_repeat1,
    ACTIONS(264), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(262), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3467] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(280), 1,
      anon_sym_DOT,
    STATE(91), 1,
      aux_sym_path_repeat1,
    ACTIONS(247), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(245), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3494] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(282), 1,
      anon_sym_DOT,
    STATE(99), 1,
      aux_sym_path_repeat1,
    ACTIONS(264), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(262), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [3521] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(196), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(198), 12,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_as,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3544] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(284), 1,
      anon_sym_DOT,
    STATE(98), 1,
      aux_sym_path_repeat1,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3571] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(287), 1,
      anon_sym_DOT,
    STATE(99), 1,
      aux_sym_path_repeat1,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [3598] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(290), 1,
      anon_sym_DOT,
    STATE(96), 1,
      aux_sym_path_repeat1,
    ACTIONS(247), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(245), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [3625] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(292), 1,
      anon_sym_DOT,
    STATE(87), 1,
      aux_sym_path_repeat1,
    ACTIONS(247), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(245), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3652] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(196), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(198), 11,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
      anon_sym_DOT,
  [3674] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(294), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
    ACTIONS(190), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(192), 9,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3698] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(196), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(198), 11,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3720] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(296), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    ACTIONS(31), 2,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
    ACTIONS(298), 2,
      sym__newline,
      anon_sym_LBRACE,
    STATE(215), 3,
      sym_major_lvl,
      sym_minor_lvl,
      sym__any_name,
    ACTIONS(13), 4,
      anon_sym_STAR,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
  [3752] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(190), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(192), 11,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3774] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 11,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3796] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 11,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3818] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(296), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    ACTIONS(31), 2,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
    ACTIONS(300), 2,
      sym__newline,
      anon_sym_LBRACE,
    STATE(215), 3,
      sym_major_lvl,
      sym_minor_lvl,
      sym__any_name,
    ACTIONS(13), 4,
      anon_sym_STAR,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
  [3850] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(304), 1,
      anon_sym_as,
    ACTIONS(302), 2,
      sym__newline,
      anon_sym_LBRACE,
    ACTIONS(308), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(306), 8,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3876] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(310), 1,
      anon_sym_COMMA,
    ACTIONS(314), 1,
      anon_sym_RBRACK,
    ACTIONS(318), 1,
      anon_sym_GT,
    ACTIONS(322), 1,
      anon_sym_SLASH,
    STATE(208), 1,
      aux_sym_attribute_constraints_repeat1,
    ACTIONS(320), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(312), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(316), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [3910] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(294), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
    ACTIONS(190), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(192), 9,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_DOT,
  [3934] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(190), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(192), 11,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
      anon_sym_DOT,
  [3956] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(257), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(255), 11,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
      anon_sym_DOT,
  [3978] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(326), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(324), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [3999] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(308), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(306), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4020] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(318), 1,
      anon_sym_GT,
    ACTIONS(322), 1,
      anon_sym_SLASH,
    ACTIONS(320), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(312), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(328), 6,
      anon_sym_COMMA,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4047] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(332), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(330), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4068] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(318), 1,
      anon_sym_GT,
    ACTIONS(322), 1,
      anon_sym_SLASH,
    ACTIONS(320), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(334), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
    ACTIONS(312), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(316), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4097] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(338), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(336), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4118] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(340), 1,
      anon_sym_COMMA,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(352), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4149] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(318), 1,
      anon_sym_GT,
    ACTIONS(322), 1,
      anon_sym_SLASH,
    ACTIONS(320), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(354), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
    ACTIONS(312), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(316), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4178] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(358), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(356), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4199] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(362), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(360), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4220] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(366), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(364), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4241] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(368), 1,
      anon_sym_COMMA,
    ACTIONS(370), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4272] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(308), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(306), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4293] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(374), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(372), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4314] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(378), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(376), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4335] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(308), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(306), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4356] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(366), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(364), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4377] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(362), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(360), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4398] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(354), 2,
      anon_sym_COMMA,
      anon_sym_RPAREN,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4427] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(386), 1,
      anon_sym_GT,
    ACTIONS(390), 1,
      anon_sym_SLASH,
    ACTIONS(380), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
    ACTIONS(388), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(382), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(384), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4456] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(378), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(376), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4477] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(398), 1,
      anon_sym_GT,
    ACTIONS(402), 1,
      anon_sym_SLASH,
    ACTIONS(392), 2,
      sym__newline,
      anon_sym_LBRACE,
    ACTIONS(400), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(394), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(396), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4506] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(374), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(372), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4527] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(358), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(356), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4548] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(338), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(336), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4569] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(332), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(330), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4590] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(328), 6,
      anon_sym_COMMA,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_RPAREN,
  [4617] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(406), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(404), 7,
      anon_sym_COMMA,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_RPAREN,
  [4642] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(338), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(336), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4663] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(332), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(330), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4684] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(326), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(324), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4705] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(366), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(364), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4726] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(408), 1,
      anon_sym_COMMA,
    ACTIONS(410), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4757] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(318), 1,
      anon_sym_GT,
    ACTIONS(322), 1,
      anon_sym_SLASH,
    ACTIONS(320), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(380), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
    ACTIONS(312), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(316), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4786] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(326), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(324), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4807] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(374), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(372), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4828] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(386), 1,
      anon_sym_GT,
    ACTIONS(390), 1,
      anon_sym_SLASH,
    ACTIONS(388), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(412), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
    ACTIONS(382), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(384), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4857] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(398), 1,
      anon_sym_GT,
    ACTIONS(402), 1,
      anon_sym_SLASH,
    ACTIONS(354), 2,
      sym__newline,
      anon_sym_LBRACE,
    ACTIONS(400), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(394), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(396), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [4886] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(362), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(360), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4907] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(322), 1,
      anon_sym_SLASH,
    ACTIONS(406), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(312), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(404), 7,
      anon_sym_COMMA,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
  [4932] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(358), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(356), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_RPAREN,
  [4953] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(378), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(376), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [4974] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(390), 1,
      anon_sym_SLASH,
    ACTIONS(406), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(382), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(404), 7,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
  [4999] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(386), 1,
      anon_sym_GT,
    ACTIONS(390), 1,
      anon_sym_SLASH,
    ACTIONS(388), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(382), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(328), 6,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5026] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(414), 1,
      anon_sym_COMMA,
    ACTIONS(416), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5057] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(374), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(372), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5078] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(378), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(376), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5099] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(308), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(306), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5120] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(366), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(364), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5141] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(362), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(360), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5162] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(358), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(356), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5183] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(386), 1,
      anon_sym_GT,
    ACTIONS(390), 1,
      anon_sym_SLASH,
    ACTIONS(354), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
    ACTIONS(388), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(382), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(384), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5212] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(326), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(324), 10,
      anon_sym_COMMA,
      anon_sym_STAR,
      anon_sym_RBRACK,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5233] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(402), 1,
      anon_sym_SLASH,
    ACTIONS(406), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(394), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(404), 7,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
  [5258] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(398), 1,
      anon_sym_GT,
    ACTIONS(402), 1,
      anon_sym_SLASH,
    ACTIONS(400), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(394), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(328), 6,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5285] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(338), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(336), 10,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5306] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(332), 3,
      anon_sym_LT,
      anon_sym_EQ,
      anon_sym_SLASH,
    ACTIONS(330), 10,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
  [5327] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(296), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    ACTIONS(31), 2,
      anon_sym_SMT_DASHlevel,
      anon_sym_SAT_DASHlevel,
    STATE(215), 3,
      sym_major_lvl,
      sym_minor_lvl,
      sym__any_name,
    ACTIONS(13), 4,
      anon_sym_STAR,
      anon_sym_group_DASHcardinality,
      anon_sym_feature_DASHcardinality,
      anon_sym_aggregate_DASHfunction,
  [5355] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(418), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5383] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(420), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5411] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(422), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5439] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(424), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5467] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(426), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5495] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(428), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5523] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(430), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5551] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 1,
      anon_sym_GT,
    ACTIONS(350), 1,
      anon_sym_SLASH,
    ACTIONS(432), 1,
      anon_sym_RPAREN,
    ACTIONS(348), 2,
      anon_sym_LT,
      anon_sym_EQ,
    ACTIONS(342), 3,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(344), 4,
      anon_sym_PIPE,
      anon_sym_AMP,
      anon_sym_EQ_GT,
      anon_sym_LT_EQ_GT,
  [5579] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(436), 1,
      anon_sym_RBRACE,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5610] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(442), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5641] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(444), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5672] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(446), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5703] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(448), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5734] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(450), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5765] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(452), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5796] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    ACTIONS(454), 1,
      anon_sym_RBRACE,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5827] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(223), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5855] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(234), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5883] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(220), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5911] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(210), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5939] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(434), 1,
      sym_name,
    ACTIONS(438), 1,
      anon_sym_constraint,
    ACTIONS(440), 1,
      anon_sym_constraints,
    STATE(25), 1,
      sym__any_name,
    STATE(35), 1,
      sym_string,
    STATE(214), 4,
      sym_attribute_constraint,
      sym_attribute_constraints,
      sym_attribute_value,
      sym__attribute,
  [5967] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(456), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(101), 1,
      sym__any_name,
    STATE(224), 1,
      sym_path,
    ACTIONS(458), 2,
      sym__newline,
      anon_sym_LBRACE,
  [5990] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(460), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(255), 1,
      sym__any_name,
    ACTIONS(462), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6010] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(227), 1,
      anon_sym_DQUOTE,
    ACTIONS(239), 1,
      sym_name,
    STATE(113), 1,
      sym_string,
    STATE(114), 1,
      sym__any_name,
  [6026] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(25), 1,
      anon_sym_DQUOTE,
    ACTIONS(241), 1,
      sym_name,
    STATE(88), 1,
      sym_string,
    STATE(89), 1,
      sym__any_name,
  [6042] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(186), 1,
      anon_sym_DQUOTE,
    ACTIONS(253), 1,
      sym_name,
    STATE(106), 1,
      sym_string,
    STATE(108), 1,
      sym__any_name,
  [6058] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(466), 1,
      anon_sym_DOT,
    STATE(202), 1,
      aux_sym_lang_lvl_repeat1,
    ACTIONS(464), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6072] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(470), 1,
      anon_sym_DOT,
    STATE(199), 1,
      aux_sym_lang_lvl_repeat1,
    ACTIONS(468), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6086] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(150), 1,
      anon_sym_DQUOTE,
    ACTIONS(251), 1,
      sym_name,
    STATE(35), 1,
      sym_string,
    STATE(107), 1,
      sym__any_name,
  [6102] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(474), 1,
      anon_sym_DOT,
    STATE(202), 1,
      aux_sym_lang_lvl_repeat1,
    ACTIONS(472), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6116] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(477), 1,
      anon_sym_COMMA,
    ACTIONS(479), 1,
      anon_sym_RBRACE,
    STATE(207), 1,
      aux_sym_attributes_repeat1,
  [6129] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(481), 1,
      anon_sym_COMMA,
    ACTIONS(483), 1,
      anon_sym_RBRACE,
    STATE(207), 1,
      aux_sym_attributes_repeat1,
  [6142] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(485), 1,
      anon_sym_COMMA,
    ACTIONS(487), 1,
      anon_sym_RBRACK,
    STATE(209), 1,
      aux_sym_vector_repeat1,
  [6155] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(489), 3,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_DOT,
  [6164] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(491), 1,
      anon_sym_COMMA,
    ACTIONS(494), 1,
      anon_sym_RBRACE,
    STATE(207), 1,
      aux_sym_attributes_repeat1,
  [6177] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(496), 1,
      anon_sym_COMMA,
    ACTIONS(498), 1,
      anon_sym_RBRACK,
    STATE(213), 1,
      aux_sym_attribute_constraints_repeat1,
  [6190] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(500), 1,
      anon_sym_COMMA,
    ACTIONS(502), 1,
      anon_sym_RBRACK,
    STATE(217), 1,
      aux_sym_vector_repeat1,
  [6203] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(504), 1,
      anon_sym_COMMA,
    ACTIONS(506), 1,
      anon_sym_RBRACE,
    STATE(203), 1,
      aux_sym_attributes_repeat1,
  [6216] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(508), 3,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_DOT,
  [6225] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(510), 1,
      anon_sym_COMMA,
    ACTIONS(512), 1,
      anon_sym_RBRACE,
    STATE(207), 1,
      aux_sym_attributes_repeat1,
  [6238] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(514), 1,
      anon_sym_COMMA,
    ACTIONS(517), 1,
      anon_sym_RBRACK,
    STATE(213), 1,
      aux_sym_attribute_constraints_repeat1,
  [6251] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(519), 1,
      anon_sym_COMMA,
    ACTIONS(521), 1,
      anon_sym_RBRACE,
    STATE(212), 1,
      aux_sym_attributes_repeat1,
  [6264] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(472), 3,
      sym__newline,
      anon_sym_LBRACE,
      anon_sym_DOT,
  [6273] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(523), 1,
      anon_sym_LBRACE,
    ACTIONS(525), 1,
      sym__newline,
    STATE(21), 1,
      sym_attributes,
  [6286] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(527), 1,
      anon_sym_COMMA,
    ACTIONS(530), 1,
      anon_sym_RBRACK,
    STATE(217), 1,
      aux_sym_vector_repeat1,
  [6299] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(532), 1,
      anon_sym_LBRACE,
    ACTIONS(534), 1,
      sym__newline,
    STATE(14), 1,
      sym_attributes,
  [6312] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(536), 1,
      anon_sym_COMMA,
    ACTIONS(538), 1,
      anon_sym_RBRACE,
    STATE(207), 1,
      aux_sym_attributes_repeat1,
  [6325] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(540), 1,
      anon_sym_COMMA,
    ACTIONS(542), 1,
      anon_sym_RBRACE,
    STATE(219), 1,
      aux_sym_attributes_repeat1,
  [6338] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(544), 1,
      anon_sym_COMMA,
    ACTIONS(546), 1,
      anon_sym_RBRACK,
    STATE(217), 1,
      aux_sym_vector_repeat1,
  [6351] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(548), 1,
      anon_sym_COMMA,
    ACTIONS(550), 1,
      anon_sym_RBRACK,
    STATE(221), 1,
      aux_sym_vector_repeat1,
  [6364] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(552), 1,
      anon_sym_COMMA,
    ACTIONS(554), 1,
      anon_sym_RBRACE,
    STATE(204), 1,
      aux_sym_attributes_repeat1,
  [6377] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(556), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6385] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(558), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6393] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(560), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6401] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(562), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6409] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(558), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6417] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(564), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6425] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(126), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6433] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(116), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6441] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(128), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6449] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(122), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6457] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(494), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6465] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(566), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6473] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(568), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6481] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(570), 1,
      anon_sym_STAR,
    ACTIONS(572), 1,
      sym_int,
  [6491] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(574), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6499] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(576), 2,
      anon_sym_STAR,
      sym_int,
  [6507] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(578), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6515] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(580), 1,
      anon_sym_DOT_DOT,
    ACTIONS(582), 1,
      anon_sym_RBRACK,
  [6525] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(584), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6533] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(562), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6541] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(586), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6549] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(530), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6557] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(564), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6565] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(574), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6573] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(126), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6581] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(116), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6589] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(128), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6597] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(588), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6605] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(590), 2,
      anon_sym_COMMA,
      anon_sym_RBRACE,
  [6613] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(592), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6621] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(122), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
  [6629] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(594), 2,
      sym__newline,
      anon_sym_LBRACE,
  [6637] = 2,
    ACTIONS(596), 1,
      aux_sym_string_token1,
    ACTIONS(598), 1,
      sym_comment,
  [6644] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(600), 1,
      ts_builtin_sym_end,
  [6651] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(602), 1,
      anon_sym_DQUOTE,
  [6658] = 2,
    ACTIONS(598), 1,
      sym_comment,
    ACTIONS(604), 1,
      aux_sym_string_token1,
  [6665] = 2,
    ACTIONS(598), 1,
      sym_comment,
    ACTIONS(606), 1,
      aux_sym_string_token1,
  [6672] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(608), 1,
      anon_sym_LBRACK,
  [6679] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(582), 1,
      anon_sym_RBRACK,
  [6686] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(610), 1,
      anon_sym_LPAREN,
  [6693] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(612), 1,
      anon_sym_RBRACK,
  [6700] = 2,
    ACTIONS(598), 1,
      sym_comment,
    ACTIONS(614), 1,
      aux_sym_string_token1,
  [6707] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(616), 1,
      anon_sym_DQUOTE,
  [6714] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(618), 1,
      anon_sym_DQUOTE,
  [6721] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(620), 1,
      anon_sym_DQUOTE,
  [6728] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(622), 1,
      anon_sym_LPAREN,
  [6735] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(624), 1,
      anon_sym_LPAREN,
  [6742] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(626), 1,
      anon_sym_LPAREN,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(13)] = 0,
  [SMALL_STATE(14)] = 37,
  [SMALL_STATE(15)] = 74,
  [SMALL_STATE(16)] = 111,
  [SMALL_STATE(17)] = 146,
  [SMALL_STATE(18)] = 181,
  [SMALL_STATE(19)] = 216,
  [SMALL_STATE(20)] = 251,
  [SMALL_STATE(21)] = 286,
  [SMALL_STATE(22)] = 323,
  [SMALL_STATE(23)] = 358,
  [SMALL_STATE(24)] = 393,
  [SMALL_STATE(25)] = 428,
  [SMALL_STATE(26)] = 492,
  [SMALL_STATE(27)] = 526,
  [SMALL_STATE(28)] = 560,
  [SMALL_STATE(29)] = 594,
  [SMALL_STATE(30)] = 628,
  [SMALL_STATE(31)] = 662,
  [SMALL_STATE(32)] = 696,
  [SMALL_STATE(33)] = 730,
  [SMALL_STATE(34)] = 764,
  [SMALL_STATE(35)] = 827,
  [SMALL_STATE(36)] = 860,
  [SMALL_STATE(37)] = 923,
  [SMALL_STATE(38)] = 956,
  [SMALL_STATE(39)] = 1019,
  [SMALL_STATE(40)] = 1068,
  [SMALL_STATE(41)] = 1131,
  [SMALL_STATE(42)] = 1191,
  [SMALL_STATE(43)] = 1251,
  [SMALL_STATE(44)] = 1311,
  [SMALL_STATE(45)] = 1363,
  [SMALL_STATE(46)] = 1415,
  [SMALL_STATE(47)] = 1464,
  [SMALL_STATE(48)] = 1513,
  [SMALL_STATE(49)] = 1562,
  [SMALL_STATE(50)] = 1611,
  [SMALL_STATE(51)] = 1660,
  [SMALL_STATE(52)] = 1709,
  [SMALL_STATE(53)] = 1758,
  [SMALL_STATE(54)] = 1807,
  [SMALL_STATE(55)] = 1856,
  [SMALL_STATE(56)] = 1905,
  [SMALL_STATE(57)] = 1954,
  [SMALL_STATE(58)] = 2003,
  [SMALL_STATE(59)] = 2052,
  [SMALL_STATE(60)] = 2101,
  [SMALL_STATE(61)] = 2150,
  [SMALL_STATE(62)] = 2199,
  [SMALL_STATE(63)] = 2248,
  [SMALL_STATE(64)] = 2297,
  [SMALL_STATE(65)] = 2346,
  [SMALL_STATE(66)] = 2395,
  [SMALL_STATE(67)] = 2444,
  [SMALL_STATE(68)] = 2493,
  [SMALL_STATE(69)] = 2542,
  [SMALL_STATE(70)] = 2591,
  [SMALL_STATE(71)] = 2640,
  [SMALL_STATE(72)] = 2689,
  [SMALL_STATE(73)] = 2738,
  [SMALL_STATE(74)] = 2787,
  [SMALL_STATE(75)] = 2836,
  [SMALL_STATE(76)] = 2885,
  [SMALL_STATE(77)] = 2934,
  [SMALL_STATE(78)] = 2968,
  [SMALL_STATE(79)] = 3001,
  [SMALL_STATE(80)] = 3034,
  [SMALL_STATE(81)] = 3067,
  [SMALL_STATE(82)] = 3100,
  [SMALL_STATE(83)] = 3133,
  [SMALL_STATE(84)] = 3166,
  [SMALL_STATE(85)] = 3199,
  [SMALL_STATE(86)] = 3232,
  [SMALL_STATE(87)] = 3260,
  [SMALL_STATE(88)] = 3288,
  [SMALL_STATE(89)] = 3311,
  [SMALL_STATE(90)] = 3334,
  [SMALL_STATE(91)] = 3361,
  [SMALL_STATE(92)] = 3388,
  [SMALL_STATE(93)] = 3413,
  [SMALL_STATE(94)] = 3440,
  [SMALL_STATE(95)] = 3467,
  [SMALL_STATE(96)] = 3494,
  [SMALL_STATE(97)] = 3521,
  [SMALL_STATE(98)] = 3544,
  [SMALL_STATE(99)] = 3571,
  [SMALL_STATE(100)] = 3598,
  [SMALL_STATE(101)] = 3625,
  [SMALL_STATE(102)] = 3652,
  [SMALL_STATE(103)] = 3674,
  [SMALL_STATE(104)] = 3698,
  [SMALL_STATE(105)] = 3720,
  [SMALL_STATE(106)] = 3752,
  [SMALL_STATE(107)] = 3774,
  [SMALL_STATE(108)] = 3796,
  [SMALL_STATE(109)] = 3818,
  [SMALL_STATE(110)] = 3850,
  [SMALL_STATE(111)] = 3876,
  [SMALL_STATE(112)] = 3910,
  [SMALL_STATE(113)] = 3934,
  [SMALL_STATE(114)] = 3956,
  [SMALL_STATE(115)] = 3978,
  [SMALL_STATE(116)] = 3999,
  [SMALL_STATE(117)] = 4020,
  [SMALL_STATE(118)] = 4047,
  [SMALL_STATE(119)] = 4068,
  [SMALL_STATE(120)] = 4097,
  [SMALL_STATE(121)] = 4118,
  [SMALL_STATE(122)] = 4149,
  [SMALL_STATE(123)] = 4178,
  [SMALL_STATE(124)] = 4199,
  [SMALL_STATE(125)] = 4220,
  [SMALL_STATE(126)] = 4241,
  [SMALL_STATE(127)] = 4272,
  [SMALL_STATE(128)] = 4293,
  [SMALL_STATE(129)] = 4314,
  [SMALL_STATE(130)] = 4335,
  [SMALL_STATE(131)] = 4356,
  [SMALL_STATE(132)] = 4377,
  [SMALL_STATE(133)] = 4398,
  [SMALL_STATE(134)] = 4427,
  [SMALL_STATE(135)] = 4456,
  [SMALL_STATE(136)] = 4477,
  [SMALL_STATE(137)] = 4506,
  [SMALL_STATE(138)] = 4527,
  [SMALL_STATE(139)] = 4548,
  [SMALL_STATE(140)] = 4569,
  [SMALL_STATE(141)] = 4590,
  [SMALL_STATE(142)] = 4617,
  [SMALL_STATE(143)] = 4642,
  [SMALL_STATE(144)] = 4663,
  [SMALL_STATE(145)] = 4684,
  [SMALL_STATE(146)] = 4705,
  [SMALL_STATE(147)] = 4726,
  [SMALL_STATE(148)] = 4757,
  [SMALL_STATE(149)] = 4786,
  [SMALL_STATE(150)] = 4807,
  [SMALL_STATE(151)] = 4828,
  [SMALL_STATE(152)] = 4857,
  [SMALL_STATE(153)] = 4886,
  [SMALL_STATE(154)] = 4907,
  [SMALL_STATE(155)] = 4932,
  [SMALL_STATE(156)] = 4953,
  [SMALL_STATE(157)] = 4974,
  [SMALL_STATE(158)] = 4999,
  [SMALL_STATE(159)] = 5026,
  [SMALL_STATE(160)] = 5057,
  [SMALL_STATE(161)] = 5078,
  [SMALL_STATE(162)] = 5099,
  [SMALL_STATE(163)] = 5120,
  [SMALL_STATE(164)] = 5141,
  [SMALL_STATE(165)] = 5162,
  [SMALL_STATE(166)] = 5183,
  [SMALL_STATE(167)] = 5212,
  [SMALL_STATE(168)] = 5233,
  [SMALL_STATE(169)] = 5258,
  [SMALL_STATE(170)] = 5285,
  [SMALL_STATE(171)] = 5306,
  [SMALL_STATE(172)] = 5327,
  [SMALL_STATE(173)] = 5355,
  [SMALL_STATE(174)] = 5383,
  [SMALL_STATE(175)] = 5411,
  [SMALL_STATE(176)] = 5439,
  [SMALL_STATE(177)] = 5467,
  [SMALL_STATE(178)] = 5495,
  [SMALL_STATE(179)] = 5523,
  [SMALL_STATE(180)] = 5551,
  [SMALL_STATE(181)] = 5579,
  [SMALL_STATE(182)] = 5610,
  [SMALL_STATE(183)] = 5641,
  [SMALL_STATE(184)] = 5672,
  [SMALL_STATE(185)] = 5703,
  [SMALL_STATE(186)] = 5734,
  [SMALL_STATE(187)] = 5765,
  [SMALL_STATE(188)] = 5796,
  [SMALL_STATE(189)] = 5827,
  [SMALL_STATE(190)] = 5855,
  [SMALL_STATE(191)] = 5883,
  [SMALL_STATE(192)] = 5911,
  [SMALL_STATE(193)] = 5939,
  [SMALL_STATE(194)] = 5967,
  [SMALL_STATE(195)] = 5990,
  [SMALL_STATE(196)] = 6010,
  [SMALL_STATE(197)] = 6026,
  [SMALL_STATE(198)] = 6042,
  [SMALL_STATE(199)] = 6058,
  [SMALL_STATE(200)] = 6072,
  [SMALL_STATE(201)] = 6086,
  [SMALL_STATE(202)] = 6102,
  [SMALL_STATE(203)] = 6116,
  [SMALL_STATE(204)] = 6129,
  [SMALL_STATE(205)] = 6142,
  [SMALL_STATE(206)] = 6155,
  [SMALL_STATE(207)] = 6164,
  [SMALL_STATE(208)] = 6177,
  [SMALL_STATE(209)] = 6190,
  [SMALL_STATE(210)] = 6203,
  [SMALL_STATE(211)] = 6216,
  [SMALL_STATE(212)] = 6225,
  [SMALL_STATE(213)] = 6238,
  [SMALL_STATE(214)] = 6251,
  [SMALL_STATE(215)] = 6264,
  [SMALL_STATE(216)] = 6273,
  [SMALL_STATE(217)] = 6286,
  [SMALL_STATE(218)] = 6299,
  [SMALL_STATE(219)] = 6312,
  [SMALL_STATE(220)] = 6325,
  [SMALL_STATE(221)] = 6338,
  [SMALL_STATE(222)] = 6351,
  [SMALL_STATE(223)] = 6364,
  [SMALL_STATE(224)] = 6377,
  [SMALL_STATE(225)] = 6385,
  [SMALL_STATE(226)] = 6393,
  [SMALL_STATE(227)] = 6401,
  [SMALL_STATE(228)] = 6409,
  [SMALL_STATE(229)] = 6417,
  [SMALL_STATE(230)] = 6425,
  [SMALL_STATE(231)] = 6433,
  [SMALL_STATE(232)] = 6441,
  [SMALL_STATE(233)] = 6449,
  [SMALL_STATE(234)] = 6457,
  [SMALL_STATE(235)] = 6465,
  [SMALL_STATE(236)] = 6473,
  [SMALL_STATE(237)] = 6481,
  [SMALL_STATE(238)] = 6491,
  [SMALL_STATE(239)] = 6499,
  [SMALL_STATE(240)] = 6507,
  [SMALL_STATE(241)] = 6515,
  [SMALL_STATE(242)] = 6525,
  [SMALL_STATE(243)] = 6533,
  [SMALL_STATE(244)] = 6541,
  [SMALL_STATE(245)] = 6549,
  [SMALL_STATE(246)] = 6557,
  [SMALL_STATE(247)] = 6565,
  [SMALL_STATE(248)] = 6573,
  [SMALL_STATE(249)] = 6581,
  [SMALL_STATE(250)] = 6589,
  [SMALL_STATE(251)] = 6597,
  [SMALL_STATE(252)] = 6605,
  [SMALL_STATE(253)] = 6613,
  [SMALL_STATE(254)] = 6621,
  [SMALL_STATE(255)] = 6629,
  [SMALL_STATE(256)] = 6637,
  [SMALL_STATE(257)] = 6644,
  [SMALL_STATE(258)] = 6651,
  [SMALL_STATE(259)] = 6658,
  [SMALL_STATE(260)] = 6665,
  [SMALL_STATE(261)] = 6672,
  [SMALL_STATE(262)] = 6679,
  [SMALL_STATE(263)] = 6686,
  [SMALL_STATE(264)] = 6693,
  [SMALL_STATE(265)] = 6700,
  [SMALL_STATE(266)] = 6707,
  [SMALL_STATE(267)] = 6714,
  [SMALL_STATE(268)] = 6721,
  [SMALL_STATE(269)] = 6728,
  [SMALL_STATE(270)] = 6735,
  [SMALL_STATE(271)] = 6742,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [7] = {.entry = {.count = 1, .reusable = false}}, SHIFT(81),
  [9] = {.entry = {.count = 1, .reusable = false}}, SHIFT(194),
  [11] = {.entry = {.count = 1, .reusable = true}}, SHIFT(237),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(206),
  [15] = {.entry = {.count = 1, .reusable = false}}, SHIFT(242),
  [17] = {.entry = {.count = 1, .reusable = true}}, SHIFT(51),
  [19] = {.entry = {.count = 1, .reusable = true}}, SHIFT(53),
  [21] = {.entry = {.count = 1, .reusable = false}}, SHIFT(263),
  [23] = {.entry = {.count = 1, .reusable = false}}, SHIFT(150),
  [25] = {.entry = {.count = 1, .reusable = true}}, SHIFT(259),
  [27] = {.entry = {.count = 1, .reusable = true}}, SHIFT(156),
  [29] = {.entry = {.count = 1, .reusable = false}}, SHIFT(253),
  [31] = {.entry = {.count = 1, .reusable = true}}, SHIFT(211),
  [33] = {.entry = {.count = 1, .reusable = false}}, SHIFT(216),
  [35] = {.entry = {.count = 1, .reusable = false}}, SHIFT(218),
  [37] = {.entry = {.count = 1, .reusable = true}}, SHIFT(30),
  [39] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [41] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(81),
  [44] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(194),
  [47] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(237),
  [50] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(206),
  [53] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(242),
  [56] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(51),
  [59] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(53),
  [62] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(263),
  [65] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(150),
  [68] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(259),
  [71] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(156),
  [74] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(253),
  [77] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(211),
  [80] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(216),
  [83] = {.entry = {.count = 1, .reusable = true}}, SHIFT(26),
  [85] = {.entry = {.count = 1, .reusable = true}}, SHIFT(33),
  [87] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [89] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(218),
  [92] = {.entry = {.count = 1, .reusable = true}}, SHIFT(29),
  [94] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [96] = {.entry = {.count = 1, .reusable = true}}, SHIFT(27),
  [98] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [100] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [102] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blk, 2, .production_id = 7),
  [104] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blk, 2, .production_id = 7),
  [106] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [108] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blk, 2, .production_id = 8),
  [110] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blk, 2, .production_id = 8),
  [112] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [114] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [116] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attributes, 4),
  [118] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_attributes, 4),
  [120] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_attributes, 3),
  [122] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attributes, 3),
  [124] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_attributes, 5, .production_id = 27),
  [126] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attributes, 5, .production_id = 27),
  [128] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attributes, 4, .production_id = 15),
  [130] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_attributes, 4, .production_id = 15),
  [132] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [134] = {.entry = {.count = 1, .reusable = false}}, SHIFT(93),
  [136] = {.entry = {.count = 1, .reusable = true}}, SHIFT(189),
  [138] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_value, 1, .production_id = 11),
  [140] = {.entry = {.count = 1, .reusable = true}}, SHIFT(41),
  [142] = {.entry = {.count = 1, .reusable = true}}, SHIFT(76),
  [144] = {.entry = {.count = 1, .reusable = true}}, SHIFT(58),
  [146] = {.entry = {.count = 1, .reusable = false}}, SHIFT(270),
  [148] = {.entry = {.count = 1, .reusable = false}}, SHIFT(160),
  [150] = {.entry = {.count = 1, .reusable = true}}, SHIFT(260),
  [152] = {.entry = {.count = 1, .reusable = true}}, SHIFT(161),
  [154] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blk, 4, .production_id = 21),
  [156] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blk, 4, .production_id = 21),
  [158] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blk, 4, .production_id = 17),
  [160] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blk, 4, .production_id = 17),
  [162] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blk, 5, .production_id = 24),
  [164] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blk, 5, .production_id = 24),
  [166] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blk, 5, .production_id = 23),
  [168] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blk, 5, .production_id = 23),
  [170] = {.entry = {.count = 1, .reusable = false}}, SHIFT(95),
  [172] = {.entry = {.count = 1, .reusable = true}}, SHIFT(193),
  [174] = {.entry = {.count = 1, .reusable = true}}, SHIFT(42),
  [176] = {.entry = {.count = 1, .reusable = true}}, SHIFT(228),
  [178] = {.entry = {.count = 1, .reusable = true}}, SHIFT(73),
  [180] = {.entry = {.count = 1, .reusable = true}}, SHIFT(49),
  [182] = {.entry = {.count = 1, .reusable = false}}, SHIFT(271),
  [184] = {.entry = {.count = 1, .reusable = false}}, SHIFT(137),
  [186] = {.entry = {.count = 1, .reusable = true}}, SHIFT(265),
  [188] = {.entry = {.count = 1, .reusable = true}}, SHIFT(135),
  [190] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__any_name, 1, .production_id = 3),
  [192] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__any_name, 1, .production_id = 3),
  [194] = {.entry = {.count = 1, .reusable = true}}, SHIFT(238),
  [196] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_string, 3),
  [198] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string, 3),
  [200] = {.entry = {.count = 1, .reusable = true}}, SHIFT(247),
  [202] = {.entry = {.count = 1, .reusable = false}}, SHIFT(92),
  [204] = {.entry = {.count = 2, .reusable = true}}, REDUCE(sym_lang_lvl, 2, .production_id = 9), REDUCE(sym_path, 2, .production_id = 9),
  [207] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_path, 2, .production_id = 9),
  [209] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_path, 2, .production_id = 9),
  [211] = {.entry = {.count = 1, .reusable = true}}, SHIFT(225),
  [213] = {.entry = {.count = 1, .reusable = true}}, SHIFT(252),
  [215] = {.entry = {.count = 1, .reusable = true}}, SHIFT(240),
  [217] = {.entry = {.count = 1, .reusable = false}}, SHIFT(100),
  [219] = {.entry = {.count = 1, .reusable = true}}, SHIFT(46),
  [221] = {.entry = {.count = 1, .reusable = true}}, SHIFT(66),
  [223] = {.entry = {.count = 1, .reusable = false}}, SHIFT(269),
  [225] = {.entry = {.count = 1, .reusable = false}}, SHIFT(128),
  [227] = {.entry = {.count = 1, .reusable = true}}, SHIFT(256),
  [229] = {.entry = {.count = 1, .reusable = true}}, SHIFT(129),
  [231] = {.entry = {.count = 1, .reusable = false}}, SHIFT(101),
  [233] = {.entry = {.count = 1, .reusable = false}}, SHIFT(89),
  [235] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_path, 3, .production_id = 15),
  [237] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_path, 3, .production_id = 15),
  [239] = {.entry = {.count = 1, .reusable = true}}, SHIFT(114),
  [241] = {.entry = {.count = 1, .reusable = true}}, SHIFT(89),
  [243] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__header, 1),
  [245] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_path, 1),
  [247] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_path, 1),
  [249] = {.entry = {.count = 1, .reusable = true}}, SHIFT(39),
  [251] = {.entry = {.count = 1, .reusable = true}}, SHIFT(107),
  [253] = {.entry = {.count = 1, .reusable = true}}, SHIFT(108),
  [255] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_path_repeat1, 2),
  [257] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_path_repeat1, 2),
  [259] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_path_repeat1, 2), SHIFT_REPEAT(197),
  [262] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_path, 2),
  [264] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_path, 2),
  [266] = {.entry = {.count = 1, .reusable = true}}, SHIFT(77),
  [268] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_path_repeat1, 2), SHIFT_REPEAT(198),
  [271] = {.entry = {.count = 1, .reusable = true}}, SHIFT(83),
  [273] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_lang_lvl_repeat1, 2), REDUCE(aux_sym_path_repeat1, 2),
  [276] = {.entry = {.count = 1, .reusable = true}}, SHIFT(84),
  [278] = {.entry = {.count = 1, .reusable = true}}, SHIFT(82),
  [280] = {.entry = {.count = 1, .reusable = true}}, SHIFT(85),
  [282] = {.entry = {.count = 1, .reusable = true}}, SHIFT(78),
  [284] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_path_repeat1, 2), SHIFT_REPEAT(201),
  [287] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_path_repeat1, 2), SHIFT_REPEAT(196),
  [290] = {.entry = {.count = 1, .reusable = true}}, SHIFT(80),
  [292] = {.entry = {.count = 1, .reusable = true}}, SHIFT(79),
  [294] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__value, 1),
  [296] = {.entry = {.count = 1, .reusable = false}}, SHIFT(215),
  [298] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_lang_lvl, 2, .production_id = 9),
  [300] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_lang_lvl, 3, .production_id = 15),
  [302] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_ref, 1, .production_id = 4),
  [304] = {.entry = {.count = 1, .reusable = true}}, SHIFT(195),
  [306] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__expr, 1),
  [308] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__expr, 1),
  [310] = {.entry = {.count = 1, .reusable = true}}, SHIFT(44),
  [312] = {.entry = {.count = 1, .reusable = true}}, SHIFT(55),
  [314] = {.entry = {.count = 1, .reusable = true}}, SHIFT(244),
  [316] = {.entry = {.count = 1, .reusable = true}}, SHIFT(70),
  [318] = {.entry = {.count = 1, .reusable = true}}, SHIFT(71),
  [320] = {.entry = {.count = 1, .reusable = false}}, SHIFT(71),
  [322] = {.entry = {.count = 1, .reusable = false}}, SHIFT(55),
  [324] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__aggregate, 4, .production_id = 16),
  [326] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__aggregate, 4, .production_id = 16),
  [328] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_constraint, 3, .production_id = 13),
  [330] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_numeric, 3, .production_id = 12),
  [332] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_numeric, 3, .production_id = 12),
  [334] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_attribute_constraints_repeat1, 2, .production_id = 18),
  [336] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_nested_expr, 3),
  [338] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_nested_expr, 3),
  [340] = {.entry = {.count = 1, .reusable = true}}, SHIFT(54),
  [342] = {.entry = {.count = 1, .reusable = true}}, SHIFT(67),
  [344] = {.entry = {.count = 1, .reusable = true}}, SHIFT(74),
  [346] = {.entry = {.count = 1, .reusable = true}}, SHIFT(75),
  [348] = {.entry = {.count = 1, .reusable = false}}, SHIFT(75),
  [350] = {.entry = {.count = 1, .reusable = false}}, SHIFT(67),
  [352] = {.entry = {.count = 1, .reusable = true}}, SHIFT(167),
  [354] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_constraint, 2, .production_id = 6),
  [356] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__aggregate, 6, .production_id = 25),
  [358] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__aggregate, 6, .production_id = 25),
  [360] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_constraint, 1),
  [362] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_constraint, 1),
  [364] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_numeric, 1, .production_id = 2),
  [366] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_numeric, 1, .production_id = 2),
  [368] = {.entry = {.count = 1, .reusable = true}}, SHIFT(50),
  [370] = {.entry = {.count = 1, .reusable = true}}, SHIFT(149),
  [372] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_bool, 1),
  [374] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_bool, 1),
  [376] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_numeric, 1),
  [378] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_numeric, 1),
  [380] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__value, 1, .production_id = 20),
  [382] = {.entry = {.count = 1, .reusable = true}}, SHIFT(61),
  [384] = {.entry = {.count = 1, .reusable = true}}, SHIFT(60),
  [386] = {.entry = {.count = 1, .reusable = true}}, SHIFT(59),
  [388] = {.entry = {.count = 1, .reusable = false}}, SHIFT(59),
  [390] = {.entry = {.count = 1, .reusable = false}}, SHIFT(61),
  [392] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__header, 1, .production_id = 1),
  [394] = {.entry = {.count = 1, .reusable = true}}, SHIFT(68),
  [396] = {.entry = {.count = 1, .reusable = true}}, SHIFT(63),
  [398] = {.entry = {.count = 1, .reusable = true}}, SHIFT(56),
  [400] = {.entry = {.count = 1, .reusable = false}}, SHIFT(56),
  [402] = {.entry = {.count = 1, .reusable = false}}, SHIFT(68),
  [404] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_equation, 3, .production_id = 13),
  [406] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_equation, 3, .production_id = 13),
  [408] = {.entry = {.count = 1, .reusable = true}}, SHIFT(69),
  [410] = {.entry = {.count = 1, .reusable = true}}, SHIFT(115),
  [412] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_constraint, 2, .production_id = 18),
  [414] = {.entry = {.count = 1, .reusable = true}}, SHIFT(62),
  [416] = {.entry = {.count = 1, .reusable = true}}, SHIFT(145),
  [418] = {.entry = {.count = 1, .reusable = true}}, SHIFT(155),
  [420] = {.entry = {.count = 1, .reusable = true}}, SHIFT(165),
  [422] = {.entry = {.count = 1, .reusable = true}}, SHIFT(120),
  [424] = {.entry = {.count = 1, .reusable = true}}, SHIFT(139),
  [426] = {.entry = {.count = 1, .reusable = true}}, SHIFT(143),
  [428] = {.entry = {.count = 1, .reusable = true}}, SHIFT(138),
  [430] = {.entry = {.count = 1, .reusable = true}}, SHIFT(170),
  [432] = {.entry = {.count = 1, .reusable = true}}, SHIFT(123),
  [434] = {.entry = {.count = 1, .reusable = false}}, SHIFT(25),
  [436] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [438] = {.entry = {.count = 1, .reusable = false}}, SHIFT(52),
  [440] = {.entry = {.count = 1, .reusable = false}}, SHIFT(261),
  [442] = {.entry = {.count = 1, .reusable = true}}, SHIFT(24),
  [444] = {.entry = {.count = 1, .reusable = true}}, SHIFT(19),
  [446] = {.entry = {.count = 1, .reusable = true}}, SHIFT(250),
  [448] = {.entry = {.count = 1, .reusable = true}}, SHIFT(230),
  [450] = {.entry = {.count = 1, .reusable = true}}, SHIFT(248),
  [452] = {.entry = {.count = 1, .reusable = true}}, SHIFT(232),
  [454] = {.entry = {.count = 1, .reusable = true}}, SHIFT(20),
  [456] = {.entry = {.count = 1, .reusable = true}}, SHIFT(101),
  [458] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_incomplete_namespace, 1),
  [460] = {.entry = {.count = 1, .reusable = true}}, SHIFT(255),
  [462] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_incomplete_ref, 2),
  [464] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_lang_lvl, 2),
  [466] = {.entry = {.count = 1, .reusable = true}}, SHIFT(109),
  [468] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_lang_lvl, 1),
  [470] = {.entry = {.count = 1, .reusable = true}}, SHIFT(105),
  [472] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_lang_lvl_repeat1, 2),
  [474] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_lang_lvl_repeat1, 2), SHIFT_REPEAT(172),
  [477] = {.entry = {.count = 1, .reusable = true}}, SHIFT(182),
  [479] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [481] = {.entry = {.count = 1, .reusable = true}}, SHIFT(185),
  [483] = {.entry = {.count = 1, .reusable = true}}, SHIFT(231),
  [485] = {.entry = {.count = 1, .reusable = true}}, SHIFT(40),
  [487] = {.entry = {.count = 1, .reusable = true}}, SHIFT(246),
  [489] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_minor_lvl, 1),
  [491] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_attributes_repeat1, 2), SHIFT_REPEAT(190),
  [494] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_attributes_repeat1, 2),
  [496] = {.entry = {.count = 1, .reusable = true}}, SHIFT(45),
  [498] = {.entry = {.count = 1, .reusable = true}}, SHIFT(251),
  [500] = {.entry = {.count = 1, .reusable = true}}, SHIFT(36),
  [502] = {.entry = {.count = 1, .reusable = true}}, SHIFT(243),
  [504] = {.entry = {.count = 1, .reusable = true}}, SHIFT(183),
  [506] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [508] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_major_lvl, 1),
  [510] = {.entry = {.count = 1, .reusable = true}}, SHIFT(186),
  [512] = {.entry = {.count = 1, .reusable = true}}, SHIFT(249),
  [514] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_attribute_constraints_repeat1, 2), SHIFT_REPEAT(64),
  [517] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_attribute_constraints_repeat1, 2),
  [519] = {.entry = {.count = 1, .reusable = true}}, SHIFT(184),
  [521] = {.entry = {.count = 1, .reusable = true}}, SHIFT(254),
  [523] = {.entry = {.count = 1, .reusable = true}}, SHIFT(192),
  [525] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [527] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_vector_repeat1, 2), SHIFT_REPEAT(43),
  [530] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_vector_repeat1, 2),
  [532] = {.entry = {.count = 1, .reusable = true}}, SHIFT(191),
  [534] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [536] = {.entry = {.count = 1, .reusable = true}}, SHIFT(181),
  [538] = {.entry = {.count = 1, .reusable = true}}, SHIFT(22),
  [540] = {.entry = {.count = 1, .reusable = true}}, SHIFT(188),
  [542] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [544] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [546] = {.entry = {.count = 1, .reusable = true}}, SHIFT(227),
  [548] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [550] = {.entry = {.count = 1, .reusable = true}}, SHIFT(229),
  [552] = {.entry = {.count = 1, .reusable = true}}, SHIFT(187),
  [554] = {.entry = {.count = 1, .reusable = true}}, SHIFT(233),
  [556] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_namespace, 2, .production_id = 5),
  [558] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_vector, 4, .production_id = 15),
  [560] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_cardinality, 3, .production_id = 10),
  [562] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_vector, 4),
  [564] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_vector, 3),
  [566] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_value, 2, .production_id = 19),
  [568] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_cardinality, 5, .production_id = 22),
  [570] = {.entry = {.count = 1, .reusable = true}}, SHIFT(262),
  [572] = {.entry = {.count = 1, .reusable = true}}, SHIFT(241),
  [574] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_vector, 5, .production_id = 27),
  [576] = {.entry = {.count = 1, .reusable = true}}, SHIFT(264),
  [578] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_constraints, 6, .production_id = 29),
  [580] = {.entry = {.count = 1, .reusable = true}}, SHIFT(239),
  [582] = {.entry = {.count = 1, .reusable = true}}, SHIFT(226),
  [584] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_constraints, 1),
  [586] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_constraints, 4, .production_id = 26),
  [588] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_constraints, 5, .production_id = 26),
  [590] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_constraints, 5, .production_id = 28),
  [592] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_group_mode, 1),
  [594] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_ref, 3, .production_id = 14),
  [596] = {.entry = {.count = 1, .reusable = false}}, SHIFT(266),
  [598] = {.entry = {.count = 1, .reusable = false}}, SHIFT_EXTRA(),
  [600] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [602] = {.entry = {.count = 1, .reusable = true}}, SHIFT(104),
  [604] = {.entry = {.count = 1, .reusable = false}}, SHIFT(267),
  [606] = {.entry = {.count = 1, .reusable = false}}, SHIFT(268),
  [608] = {.entry = {.count = 1, .reusable = true}}, SHIFT(47),
  [610] = {.entry = {.count = 1, .reusable = true}}, SHIFT(72),
  [612] = {.entry = {.count = 1, .reusable = true}}, SHIFT(236),
  [614] = {.entry = {.count = 1, .reusable = false}}, SHIFT(258),
  [616] = {.entry = {.count = 1, .reusable = true}}, SHIFT(102),
  [618] = {.entry = {.count = 1, .reusable = true}}, SHIFT(97),
  [620] = {.entry = {.count = 1, .reusable = true}}, SHIFT(37),
  [622] = {.entry = {.count = 1, .reusable = true}}, SHIFT(65),
  [624] = {.entry = {.count = 1, .reusable = true}}, SHIFT(57),
  [626] = {.entry = {.count = 1, .reusable = true}}, SHIFT(48),
};

#ifdef __cplusplus
extern "C" {
#endif
void *tree_sitter_uvl_external_scanner_create(void);
void tree_sitter_uvl_external_scanner_destroy(void *);
bool tree_sitter_uvl_external_scanner_scan(void *, TSLexer *, const bool *);
unsigned tree_sitter_uvl_external_scanner_serialize(void *, char *);
void tree_sitter_uvl_external_scanner_deserialize(void *, const char *, unsigned);

#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_uvl(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .keyword_lex_fn = ts_lex_keywords,
    .keyword_capture_token = sym_name,
    .external_scanner = {
      &ts_external_scanner_states[0][0],
      ts_external_scanner_symbol_map,
      tree_sitter_uvl_external_scanner_create,
      tree_sitter_uvl_external_scanner_destroy,
      tree_sitter_uvl_external_scanner_scan,
      tree_sitter_uvl_external_scanner_serialize,
      tree_sitter_uvl_external_scanner_deserialize,
    },
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
